#!/usr/bin/env python3
# coding=utf-8
import numpy as np
import argparse
from sklearn.metrics.ranking import _binary_clf_curve
from degnlib.degnutil import read_write as rw, array_util as ar


def get_parser():
    parser = argparse.ArgumentParser(
        description="Plot fraction of positives discovered as function of threshold along x-axis.")
    parser.add_argument("path", nargs="+", 
                        help="File(s) with score values where a curve is made for each file.")
    parser.add_argument("-out", "--outfile", help="Save plot to file (default is printing to screen).")
    parser.add_argument("-title", "--title", default="Edge discovery")
    parser.add_argument("-leg", "--legend", nargs="+", help="The legend text for each curve.")
    return parser
    
    
def get_args():
    return get_parser().parse_args()


def plot_curves(xs, ys, legends, xlim, ylim):
    if ar.ndim(xs) == 1: xs = [xs]
    if ar.ndim(ys) == 1: ys = [ys]
    if ar.ndim(legends) == 0: legends = [legends]
    
    colors = ['darkorange', 'navy', 'forestgreen', 'maroon', 'purple', 'gold', 'blue', 'black']
    
    for x, y, legend, color in zip(xs, ys, legends, colors):
        plt.step(x, y, where='post', color=color, label=legend)
    
    # the x for each curve where the y reaches zero for the first time
    if xlim is None: xlim = [0., max(x[np.argmin(y)] for x, y in zip(xs, ys))]
    if ylim is None: ylim = [0., max(np.max(y) for y in ys)]
    elif ar.ndim(ylim) == 0 or len(ylim) == 1: ylim = [0.0, ylim]
    
    xpad, ypad = .01, .01
    plt.xlim([xlim[0] - xpad, xlim[1] + xpad])
    plt.ylim([ylim[0] - ypad, ylim[1] + ypad])
    plt.xlabel('threshold')
    plt.ylabel('edges')
    plt.legend(loc="upper right")


def plot(xs, ys, legends=None, title=None, path=None, xlim=None, ylim=None):
    plt.figure()
    plot_curves(xs, ys, legends, xlim, ylim)
    if title is not None: plt.title(title)
    if path is None: plt.show()
    else:
        plt.savefig(path)
        plt.clf()


def main(args):
    legends = args.legend if args.legend is not None else [str(i) for i in range(len(args.path))]
    score_arrays = [np.asarray(rw.read_array(p)).ravel() for p in args.path]

    xs, ys = [], []
    for scores in score_arrays:
        _, tps, thres = _binary_clf_curve(np.ones(len(scores)), abs(scores))
        xs.append(thres)
        ys.append(tps / max(tps))

    plot(xs, ys, legends, args.title, args.outfile, ylim=min(np.max(y) for y in ys))


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    main(get_args())




