#!/usr/bin/env bash
echo "install submodule"
git submodule init
git submodule update
echo "install miniconda (or anaconda) if not already done."
echo "create a python 3.5 (or 3.4) environment e.g."
echo "conda create -n py35 python=3.5"
echo "activate that environment e.g."
echo "conda activate py35"

echo "install packages..."
conda install numpy scipy pandas cython theano mkl matplotlib colorama prompt_toolkit
pip install fuzzyfinder
echo "note if you try to install pytest it will change the other packages and break the program"

echo "on mac you will need some xcode command line tools..."
xcode-select --install
echo "add the knocknet folder to PATH and PYTHONPATH variables in your ~/.bashrc or ~/.bash_profile"
echo "troubleshooting"
echo "if there is a problem with theano not running, install newest package found in folder:"
ls /Library/Developer/CommandLineTools/Packages/

echo "if there are mkl/blas problems, (e.g. a popup) try using another blas (blas is the math backend dealing with linear algebra/vectors/matrices)"
echo "e.g. set flag in ~/.theanorc like so:"
echo "touch ~/.theanorc; echo -e \"[blas]\\nldflags = -lgfortran\" >> ~/.theanorc"
echo 'for more troubleshooting visit http://deeplearning.net/software/theano/troubleshooting.html'
