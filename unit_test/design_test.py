#!/usr/bin/env python3
from utilities import design as de

"""
This script is for testing if design functions are working.
"""


def test_color_cell_back():
    assert isinstance(de.color_cell_back(4), str)
    assert isinstance(de.color_cell_back(0.1), str)