#!/usr/bin/env python3
import gnw

"""
This script is for testing if design functions are working.
"""


def test_random_modules():
    assert gnw.Gene.random_modules([], []) == []


def test_random_alpha():
    assert gnw.Gene.random_alpha([]) == [1]


def test_f():
    assert gnw.Gene([], [1]).f(None) == 1