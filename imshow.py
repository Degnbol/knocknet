#!/usr/bin/env python3
import sys
import argparse
import numpy as np
from matplotlib.colors import Normalize
from degnlib.degnutil import read_write as rw, input_output as io
from pathlib import Path
import matplotlib
if sys.platform == "darwin": matplotlib.use("TkAgg")
from matplotlib import pyplot as plt

def get_parser():
    parser = argparse.ArgumentParser(description="Plot a matrix.")
    parser.add_argument("infile", metavar="filename", help="A matrix file to plot.")
    parser.add_argument("-out", "--outfile", metavar="filename",
                        help="Filename for the saved plot. Default is infile with ending .pdf.")
    parser.add_argument("-title", "--title", help="A title to add to the image")
    
    return parser


def get_args(args=None):
    args = get_parser().parse_args(args)
    if not args.outfile: args.outfile = Path(args.infile).with_suffix('.pdf')
    return args


def plot(array, title='', bar=True):
    """
    Use matplotlib pyplot imshow using a nice coloring system.
    :param array: array to plot, e.g. numpy array
    :param title: title of plot, empty to have no title
    :param bar: bool indicating if there should be a color bar
    :return: 
    """
    if title: plt.title(title)
    imshow(array)
    if bar: plt.colorbar()


def imshow(array):
    """
    Use matplotlib pyplot imshow using a nice coloring system.
    :param array: array to plot, e.g. numpy array
    :return: 
    """
    class MidpointNormalize(Normalize):
        def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
            self.midpoint = midpoint
            Normalize.__init__(self, vmin, vmax, clip)

        def __call__(self, value, clip=None):
            # ignoring masked values and all kinds of edge cases to make a simple example...
            x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
            return np.ma.masked_array(np.interp(value, x, y))
        
    plt.imshow(array, norm=MidpointNormalize(midpoint=0), cmap=plt.cm.seismic)


def write(path):
    """
    Write plot to path
    :param path: path or string filename
    :return: None
    """
    plt.savefig(str(path))
    plt.clf()


def main(args):
    mat = rw.read_array(args.infile)
    plot(mat, args.title)
    write(args.outfile)


if __name__ == '__main__':
    io.log.info("Plotting a matrix...")
    main(get_args())
