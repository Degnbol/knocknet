#!/usr/bin/env python3
import pandas as pd
from degnlib.degnutil import input_output as io, theano_util as thea, neural_network as ne
import configurations as config
from theanonet import Theanonet
from knocknet import main


class Theanostdnet(Theanonet):
    
    repeats = 10

    def define_network_function(self):
        self.train_func = []
        self.weights = []
        
        for rep in range(self.repeats):
            inputs, weights, pred, target = self.network()
            self.weights.append(weights)
            self.train_func.append(ne.training_function(inputs, weights, pred, target,
                                                        self.args.gradient_descent, self.args.lasso))
            self.predict_func = ne.prediction_function(inputs, pred)

    
    def training_function(self):
        outputs = ne.train_std(self.train_func, [self.arrays['pk_ko'], self.arrays['tf_ko']], self.arrays['gene'],
                           self.weights, config.epoch, self.args.epochs, self.args.n_print_training, self.args.batch_size)
        # update current epoch for continued training
        config.epoch += self.args.epochs

        io.log.info("update the list of arrays with the trained weight values")
        for weight in self.weights[0]:
            self.arrays[str(weight)] = thea.variable_to_array(weight)

        if "training" in self.arrays:
            self.arrays["training"] = pd.concat((self.arrays["training"], outputs))
        else:
            self.arrays["training"] = outputs


    

if __name__ == '__main__':
    # instantiate a new Theanostdnet 
    main(Theanostdnet())

