#!/usr/bin/env python3
import argparse
from pathlib import Path
import numpy as np
import pandas as pd
import theano as th
import theano.tensor as T
from theano.tensor.nlinalg import AllocDiag
from utilities import argument_parsing as argue2
from degnlib.degnutil import (array_util as ar, read_write as rw, argument_parsing as argue, input_output as io,
                              pandas_util as panda, theano_util as thea, gradient_descent as gd)
import configurations as config
from knocknet import Knocknet, main
import logging
th.config.floatX = floatX = "float32"
np.set_printoptions(suppress=False)


class Ebernet(Knocknet):
    
    # static and constant
    weight_keys = ['weight']
    mask_keys = ['mask']
    data_keys = ['ko', 'gene', 'tf_ko', 'pk_ko']
    
    
    def __init__(self):
        super().__init__()
        # the shared variable version of self.arrays['weight']
        self.W = None
        self.train_func = None
        self.predict_func = None
        self.n_pk, self.n_tf = None, None
    
    
    # argparse

    def get_commandline_functions(self):
        """
        The parsers for the commands/functions available from commandline only
        :return: 
        """
        menus = {"run": self.get_parser()}
        return {**menus, **super().get_commandline_functions()}
    
    
    def get_menu_functions(self):
        """
        The parsers only used within the program to run all menu functions and settings
        :return: None
        """
        menus = {"read-arrays": self.get_read_arrays_parser(), "init": self.get_initialize_parser(), 
                 "check": self.get_check_parser(), "define-network": self.get_define_network_parser(), 
                 "train": self.get_training_parser(), "predict": self.get_prediction_parser(),
                 "synth": self.get_synthesize_parser()}
        return {**menus, **super().get_menu_functions()}
    
    
    def get_array_parser(self):
        parser = super().get_array_parser()
        parser.add_argument("-weights", "--weights", dest="name", action="append_const", const=self.weight_keys, default=[],
                            help="add weights to the list of arrays to use.")
        parser.add_argument("-masks", "--masks", dest="name", action="append_const", const=self.mask_keys, default=[],
                            help="add masks to the list of arrays to use.")
        return parser
    
    
    def get_read_arrays_parser(self):  
        """
        Parser that manages all the inputs to read all the necessary files for this network setup.
        :return: argparse.ArgumentParser 
        """
        parser = argparse.ArgumentParser(add_help=False, 
                                         description="Read in the necessary arrays to use in the theano network.")
        parser.set_defaults(function=self.read_arrays_function)
    
        parser.add_argument("-ko", "--ko", metavar="filename",
                            help="The filename for a file with a matrix of knockouts where each row is a datapoint. "
                                 "This should include both TFs and PKs, with TF rows before PK rows.")
        parser.add_argument("-pk-ko", "--pk-ko", metavar="filename",
                            help="The filename for a file with a matrix of PK knockouts where each row is a datapoint.")
        parser.add_argument("-tf-ko", "--tf-ko", metavar="filename",
                            help="The filename for a file with a matrix of TF knockouts where each row is a datapoint.")
        parser.add_argument("-gene", "--gene", metavar="filename",
                            help="The filename for a file with a matrix of gene expression levels relative to wild-type. "
                                 "Each row is a datapoint. ")
        
        return parser
    
    
    def get_initialize_parser(self):
        """
        :return: argparse.ArgumentParser 
        """
        parser = argparse.ArgumentParser(add_help=False,
                                         description="Initialize arrays for training, i.e. define arrays not read.")
        parser.set_defaults(function=self.initialize_function)
        parser.add_argument("-n-pk", "--n-pk", type=int, help="Number of protein kinases. n_pk + n_tf = n_gene.")
        parser.add_argument("-n-tf", "--n-tf", type=int, help="Number of transcription factors. n_pk + n_tf = n_gene.")

        return parser
    
    
    def get_check_parser(self):
        """
        :return: argparse.ArgumentParser 
        """
        parser = argparse.ArgumentParser(add_help=False,
                                         description="Check arrays (weights), are masks working?")
        parser.set_defaults(function=self.check_weights_function)

        return parser

    
    def get_define_network_parser(self):
        """
        :return: argparse.ArgumentParser 
        """
        parser = argparse.ArgumentParser(add_help=False, description="Define network structure.")
        parser.set_defaults(function=self.define_network_function)
        argue2.gradient_descent(parser)
        argue2.lasso(parser)
        argue2.regularization(parser)
        # batch size cannot be dynamic, at least it is not implemented
        argue2.batch_size(parser)
        parser.add_argument("-n-pk", "--n-pk", type=int, help="Number of protein kinases. n_pk + n_tf = n_gene.")
        parser.add_argument("-n-tf", "--n-tf", type=int, help="Number of transcription factors. n_pk + n_tf = n_gene.")
        parser.add_argument("-maskw", "--mask-w", action="store_true", help="Mask W using U_k instead of only B.")
        return parser
    

    def get_training_parser(self):
        """
        The parser that has arguments for training only.
        :return: 
        """
        parser = argparse.ArgumentParser(add_help=False, formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                         description="Parse arguments that can describe how training should be performed.")
        parser.set_defaults(function=self.training_function)
    
        argue2.epoch(parser)
        argue2.n_print_training(parser)
        # batch size cannot be dynamic, at least it is not implemented
        argue2.batch_size(parser)
        
        return parser


    def get_prediction_parser(self):
        """
        The parser that has arguments for prediction only.
        :return: 
        """
        parser = argparse.ArgumentParser(add_help=False,
                                         description="Run prediction given the network has been defined with trained weights.")
        parser.set_defaults(function=self.prediction_function)
        return parser
        
    
    def get_edge_parser(self):
        """
        Add an argument to the parent edge parser 
        so we don't have to write names of all the arrays we are interested in.
        :return: argparse.ArgumentParser
        """
        parser = super().get_edge_parser()
        parser.add_argument("-weight", "--weight", dest="name", action="append_const", const=["weight"],
                            default=[], help="Create all edges needed for a full network.")
        return parser
    
    
    def get_node_parser(self):
        """
        Add an argument to the parent node parser 
        so we don't have to write names of all the arrays we are interested in.
        :return: argparse.ArgumentParser
        """
        parser = super().get_node_parser()
        parser.add_argument("-net", "--network", dest="name", action="append_const", const=self.data_keys, default=[],
                            help="Create all edges needed for a full network.")
        return parser
    
    
    def get_synthesize_parser(self):
        parser = argparse.ArgumentParser(
            add_help=False, 
            description="Given edges for the net stored in self.arrays['weight'], "
                        "calculate node values iteratively until convergence or until stopped.")
        parser.set_defaults(function=self.synthesize_function)
        parser.add_argument("-tol", "--tolerance", type=float, default=1e-12,
                            help="How much different a value is before we say that they are the same and convergence is reached.")
        parser.add_argument("-its", "--iterations", type=int, default=10000, help="Upper limit for number of iterations to perform.")

        return parser
    
    
    def get_parser(self):
        """
        Get all the needed arguments to run both reading, training and writing functionality in one go.
        :return: 
        """
        subparsers = [self.get_read_arrays_parser(), self.get_define_network_parser(), self.get_training_parser()]
        parser = argparse.ArgumentParser(add_help=False, parents=subparsers, conflict_handler='resolve',
                                         description="Ebernet. Read arrays, initialize, check weights, print info, define network, "
                                                     "train, predict, create node and edge tables, and write them to file.")
        parser.set_defaults(function=self.main_function)
        argue.force(parser)
        parser.add_argument("-thres", "--threshold", type=float, default=0.001,
                            help="The threshold for ignoring weak edge connections.")

        return parser
    
    
    # functions

    def main_function(self):
        print("read matrices")
        self.read_arrays_function()
        self.initialize_function()
        self.check_weights_function()
        self.info_function()
        print("define network")
        self.define_network_function()
        print("run training")
        self.training_function()
        print("run prediction")
        self.prediction_function()
        print("make edges and ndoes")
        command = "-net"
        self.node_function(argue.parse_args(self.get_node_parser(), command))
        if self.args.threshold: command += " -thres " + str(self.args.threshold)
        self.edge_function(argue.parse_args(self.get_edge_parser(), command))
        print("write edges and nodes")
        command = "edges nodes -path edges.csv nodes.csv -sep ,"
        if self.args.force: command += " --force"
        self.args = argue.parse_args(self.get_write_parser(), command)
        self.write_function()
    
    
    def read_arrays_function(self):
        """
        Read matrices if a path is given in parsed arguments.
        """
        def get_filename(key):
            # use what the user provided
            try: filename = getattr(self.args, key)
            except AttributeError: filename = None
            if filename is not None: return filename
            # otherwise we find default
            return key + config.array_suffix
        
        messages = ["trained weights matrices...", "weights matrix masks...", "KO datapoints..."]
        
        for keys, message in zip([self.weight_keys, self.mask_keys, self.data_keys], messages):
            io.log.info(message)
            for key in keys:
                if key in self.arrays:
                    io.log.info("array {} already read, skipping".format(key))
                else:
                    filename = get_filename(key)
                    if Path(filename).exists():
                        print("reading", key, filename)
                        self.arrays[key] = rw.read_array(filename)
        
        # use tf_ko and pk_ko if given instead of ko
        if "tf_ko" in self.arrays and "pk_ko" in self.arrays and "ko" not in self.arrays:
            self.arrays["ko"] = panda.concat([self.arrays["tf_ko"], self.arrays["pk_ko"]])
        
    
    def initialize_function(self):
        """
        Make sure arrays are set up correctly to use in the network.
        Unless quiet this will also print a few messages.
        """
        self.parse_n_tf_pk()
        self.initialize_weight()
        self.arrays['weight'] = self.mask(self.arrays['weight'])
        # not so necessary but let's add it for good measure
        self.arrays['mask'] = self.get_mask()
    
    
    def mask(self, weight):
        """
        Mask the weight.
        """
        # make mask matrix if not already loaded.
        # the masks work by being the starting point of the weights, 
        # the zero values cannot be trained since there is no gradient for a value of zero.
        weight = mask(weight)
        io.log.info("self connections removed (diagonal of weight matrix)")
        if "mask" in self.arrays:
            weight = mask(weight, self.arrays["mask"])
            io.log.info("mask applied")
        if "TF_mask" in self.arrays:
            weight = mask_TF(weight, self.arrays["TF_mask"])
            io.log.info("TF mask applied")
        if "PK_mask" in self.arrays:
            weight = mask_TF(weight, self.arrays["PK_mask"])
            io.log.info("PK mask applied")
        return weight
    
    
    def initialize_weight(self):
        """
        Initialize random values in weights if they are not already present
        """
        if 'weight' not in self.arrays:
            io.log.info("initializing random values in weight matrix")
            # using the masks here as the starting point effectively limiting which weights can be nonzero
            self.arrays['weight'] = weight(self.n_tf + self.n_pk)
            self.set_W()
        # update B
        if self.W is None: self.set_W()
        self.arrays["B"] = B(self.W, I_TF(self.n_tf, self.n_pk), I_PK(self.n_tf, self.n_pk)).eval()
    
    
    def info_function(self):
        """
        Print some general info and quality checks
        """
        super().info_function()
        print("num PKs =", self.n_pk)
        print("num TFs =", self.n_tf)
        print("num genes =", self.get_n_gene())
        print("num datapoints =", self.get_n_knockouts())
        print("num parameters = %d" % self.get_n_parameters())
        if "gene" in self.arrays: print("gene shape:", self.arrays["gene"].shape)
        if "ko" in self.arrays: print("ko shape:", self.arrays["ko"].shape)
        if "weight" in self.arrays: print("weight shape:", self.arrays["weight"].shape)
    
    
    def check_weights_function(self):
        """
        Verify the quality of the weights arrays.
        """
        # check weights
        zeros_index = self.get_mask() == 0
        fraction_is_zero = panda.sum(panda.index(self.arrays['weight'], zeros_index) == 0) / panda.sum(zeros_index)
        level = logging.ERROR if fraction_is_zero < 1 else logging.INFO
        io.log.log(level, "values that are zero when supposed to = {:.2f} %".format(fraction_is_zero.item() * 100))
    

    def define_network_function(self):
        # manage args
        regularizations = {"l1": ar.l1, "mm": ar.mm_norm}
        reg = regularizations[self.args.regularization]
        if not self.args.batch_size: self.args.batch_size = self.get_n_knockouts()

        self.parse_n_tf_pk()
        
        self.set_W()
        self.train_func = train_function(self.n_tf, self.n_pk, self.W, self.args.gradient_descent, self.args.lasso, reg, self.args.mask_w)
        self.predict_func = predict_function(self.n_tf, self.n_pk, self.W, self.args.mask_w)
    
    
    def training_function(self):
        assert self.arrays['gene'].shape == self.arrays['ko'].shape
        assert self.arrays['weight'].shape[0] == self.arrays['gene'].shape[0], \
            "weight: {}, gene: {}".format(self.arrays['weight'].shape, self.arrays['gene'].shape)
        
        # manage args
        if not self.args.batch_size: self.args.batch_size = self.get_n_knockouts()
        # NOTE: x and ko are assumed to be column vectors of experiments aka. row vectors of genes, hence axis=1
        # run training
        outputs = thea.train(self.train_func, [self.arrays['ko']], self.arrays['gene'], 
                           config.epoch, self.args.epochs, self.args.prints, self.args.batch_size, axis=1)
        # update current epoch for continued training
        config.epoch += self.args.epochs
        
        io.log.info("update the list of arrays with the trained weights values")
        self.arrays["weight"] = thea.variable_to_array(self.W)
        self.arrays["B"] = B(self.W, I_TF(self.n_tf, self.n_pk), I_PK(self.n_tf, self.n_pk)).eval()
        
        
        if "training" in self.arrays:
            self.arrays["training"] = pd.concat((self.arrays["training"], outputs))
        else:
            self.arrays["training"] = outputs


    def prediction_function(self):
        """
        Perform prediction given a defined and trained network
        :return: 
        """
        assert self.arrays['gene'].shape == self.arrays['ko'].shape
        assert self.arrays['weight'].shape[0] == self.arrays['gene'].shape[0], \
            "weight: {}, gene: {}".format(self.arrays['weight'].shape, self.arrays['gene'].shape)

        self.arrays["gene_pred"] = self.predict_func(self.arrays['ko'], self.arrays['gene'])
    
    
    def synthesize_function(self):
        """
        Given knockouts and edges for the net, calculate node values iteratively until convergence or until stopped.
        This function is similar to predict, except it keeps going and assumes the given weights are true.
        :return: 
        """
        if not self.predict_func:
            return print("run define-network first to define prediction function")
        
        self.set_W()
        self.arrays["gene"] = np.zeros(self.arrays['ko'].shape, dtype=floatX)
        diffs = []
        for i in range(self.args.iterations):
            last = self.arrays["gene"]
            self.arrays["gene"] = self.predict_func(self.arrays['ko'], self.arrays["gene"])
            diff = abs(self.arrays["gene"] - last).mean()
            diffs.append(diff)
            if diff < self.args.tolerance:
                io.log.info("Mean changes: " + str(np.asarray(diffs)))
                io.log.info("Reached convergence")
                return
            elif diff > 1e10:
                io.log.info("Mean changes: " + str(np.asarray(diffs)))
                io.log.warning("Diverges")
                # we don't keep gene matrix that has diverged
                del self.arrays["gene"]
                return
        io.log.info("Mean changes: " + str(np.asarray(diffs)))
        io.log.warning("Reached max iterations")
    
    
    # methods
    
    
    def parse_n_tf_pk(self):
        self.set_n_tf(self.args.n_tf)
        self.set_n_pk(self.args.n_pk)
        # fall back on finding it from tf_ko and pk_ko files
        if not self.n_tf:
            try: self.n_tf = self.arrays["tf_ko"].shape[0]
            except KeyError:
                raise argparse.ArgumentTypeError("n_tf could not be determined")
        if not self.n_pk:
            try: self.n_pk = self.arrays["pk_ko"].shape[0]
            except KeyError:
                raise argparse.ArgumentTypeError("n_pk could not be determined")
    
    
    def shared(self, key):
        return th.shared(np.asarray(self.arrays[key], dtype=floatX), key)
    
    
    # setters
    
    def set_n_pk(self, val):
        """
        Set n_pk and if possible n_tf using n_pk
        """
        if val is None: return
        self.n_pk = val
        if self.n_tf is None:
            self.set_n_tf(self.get_n_gene() - val)
    
    
    def set_n_tf(self, val):
        """
        Set n_pk and if possible n_tf using n_pk
        """
        if val is None: return
        self.n_tf = val
        if self.n_pk is None:
            self.set_n_pk(self.get_n_gene() - val)
    
    
    def set_W(self, key='weight'):
        self.W = self.shared(key)
    
    
    # getters
    
    def get_mask(self):
        return self.mask(np.ones(self.arrays['weight'].shape))
    
    
    def get_n_gene(self):
        """
        Get the number of genes that are measured
        :return: int 
        """
        try: return self.arrays['gene'].shape[0]
        except KeyError: return self.arrays['ko'].shape[0]
    
    
    def get_n_knockouts(self):
        """
        Get the number of datapoints (knockouts)
        :return: int 
        """
        try: return self.arrays['ko'].shape[1]
        except KeyError: return self.arrays['gene'].shape[1]
    
    
    def get_n_parameters(self):
        """
        Count number of trainable parameters by summing over all nonzero values in all masks.
        :return: int
        """
        return panda.sum(self.get_mask())


def I_TF(n_TF, n_PK):
    """
    Assuming nodes are sorted to have TF and then PK we get I_TF which is the mask for TF rows.
    :param n_TF: Number of TF
    :param n_PK: Number of PK
    :return: array with ones in diagonal for each TF and zeros for each PK
    """
    out = np.eye(n_TF + n_PK, dtype=floatX)
    out[n_TF:, n_TF:] = 0
    return out
    

def I_PK(n_TF, n_PK):
    """
    Assuming nodes are sorted to have TF and then PK we get I_PK which is the mask for PK rows.
    :param n_TF: Number of TF
    :param n_PK: Number of PK
    :return: array with ones in diagonal for each PK and zeros for each TF
    """
    out = np.eye(n_TF + n_PK, dtype=floatX)
    out[:n_TF, :n_TF] = 0
    return out


def weight(n):
    """
    Simply a matrix of random normal values where std decreases with size.
    :param n: size of both shapes of the array
    :return: array with gaussian distributed random values
    """
    return np.random.normal(scale=1. / n, size=(n, n)).astype(dtype=floatX, copy=False)


def mask(array, mask=None):
    """
    Remove diagonal in an array or use a masking array.
    :param array: array to edit.
    :param mask: optional mask to use, default is removing diagonal.
    :return: the given array now with zeros inserted according to masking.
    """
    if mask is None: np.fill_diagonal(array, 0)
    else: array[mask == 0] = 0
    return array


def mask_TF(weight, mask):
    """
    Disallow connections in weight matrix based on TF mask.
    It is assumed that values in weight matrix is ordered with TF first and PK thereafter (along axis 1).
    :param weight: matrix where some values will be set to zero
    :param mask: Mask that is used to set values to zero.
    :return: weight matrix with some masked values set to zero.
    """
    _weight = weight[:mask.shape[0], :mask.shape[1]]
    _weight[~mask] = 0
    weight[:mask.shape[0], :mask.shape[1]] = _weight
    return weight


def mask_PK(weight, mask):
    """
    Disallow connections in weight matrix based on PK mask.
    It is assumed that values in weight matrix is ordered with TF first and PK thereafter (along axis 1).
    :param weight: matrix where some values will be set to zero
    :param mask: Mask that is used to set values to zero.
    :return: weight matrix with some masked values set to zero.
    """
    _weight = weight[-mask.shape[0]:, -mask.shape[1]:]
    _weight[~mask] = 0
    weight[-mask.shape[0]:, -mask.shape[1]:] = _weight
    return weight


def B_k(W, I_TF, I_PK, U_k):
    """
    For when we want to mask W and not just B
    Get B from W where a knockout is present
    :param W: shared variable 
    :param I_TF: 
    :param I_PK: 
    :param U_k: diagonal matrix with ones except for knockouts
    :return: U_k W I_TF (2I - U_k W I_PK)^-1
    """
    return B(U_k.dot(W), I_TF, I_PK)


def B(W, I_TF, I_PK, remove_diag=False):
    """
    Get B from W
    :param W: shared variable 
    :param I_TF: 
    :param I_PK: 
    :return: W I_TF (2I - W I_PK)^-1
    :return: W I_TF (2I - 1/2 W I_PK W)^-1
    """
    I = thea.eye(W.shape[0], I_PK.shape[1])
    _B = W.dot(I_TF).dot(thea.inv(I - W.dot(I_PK)))
    # B cannot have values in the diagonal according to paper.
    # not sure if it should be removed or not. some indication it should
    if remove_diag: return thea.fill_diagonal(_B, 0)
    return _B


def c_k(ko_k):
    # how a knockout is represented as log fold change value
    # 1 -> -1, 0 -> 0
    return (4 + np.random.randn(len(ko_k))) * ko_k


def C(ko):
    # how all knockouts are represented as log fold change value
    # 1 -> -4, 0 -> 0
    return -4 * ko



def U_k(ko_k):
    return AllocDiag()(1 - ko_k)


def U(ko):
    """
    
    :param ko: 
    :return: matrix U where each column is the U_k, so no longer the diag matrix as in lit. 
    """
    return 1 - ko


def Ue_k(x_k, B_k, c_k, U_k):
    """
    Get the error for a single experiment
    :param x_k: 
    :param B_k: 
    :param c_k: 
    :param U_k: 
    :return: 
    """
    return (T.eye(B_k.shape[0]) - U_k.dot(B_k)).dot(x_k) - c_k


def UBX(X, W, I_TF, I_PK, ko, mask_w=False):
    """
    Get U_k @ B @ x for each (U_k,x) 
    :param X: columns of observations aka matrix
    :param W: trainable weights
    :param I_TF: 
    :param I_PK: 
    :param ko: 
    :return: 
    """
    if mask_w:
        Bs = [B_k(W, I_TF, I_PK, U_k(ko[:,k])) for k in range(13)]
        return thea.super_dot(Bs, X)
    else:
        _BX = B(W, I_TF, I_PK).dot(X)
        return U(ko) * _BX


def UE(X, BX, ko):
    """
    :param X: x_k column vectors aka X matrix
    :param BX: 
    :param ko: ko_k column vectors aka ko matrix 
    :return: e_k error vectors aka E matrix
    """
    return X - C(ko) - BX


def x_k(x_k, B, c_k, U_k, e=None):
    """
    Get a prediction/next time step of gene expression x using gene expression x.
    :param x_k: node values
    :param B: edge matrix for direct effects
    :param c_k: intervention vector with the knockout value(s)
    :param U_k: mask matrix that removes effects onto genes that are knocked out
    :param e: optional error vector describing the general offset in data due to latent unknown variables and confounders.
    If it is used it should be the same e_k for all k.
    :return: x_k prediction that ideally should be the same as the given x_k
    """
    out = U_k.dot(B.dot(x_k)) + c_k
    if e is None: return out
    return out + U_k.dot(e)


def X(BX, ko):
    """
    Get a prediction/next time step of gene expression x using gene expression x.
    :param BX: 
    :param ko: 
    :return: 
    """
    return BX + C(ko)


def loss(UE, weight=None, lasso=0.0, reg=thea.l1):
    """
    Get loss with optional l-norm 1 included
    :param UE: variable error column vectors aka matrix
    :param weight: variable matrix describing weights which can be used for regularization
    :param lasso: coefficient (often called lambda) used on regularization
    :param reg: regularization function, default is l1-norm
    :return: list of losses, either [l2-norm, l2-norm] or [l2-norm + regularization, l2-norm]
    """
    l2 = thea.l2(UE)
    if not lasso or not weight: return [l2, l2]
    return [l2 + lasso * reg(weight), l2]


def train_function(n_TF, n_PK, W, descent, lasso, reg, mask_w=False):
    x, ko = T.fmatrix("x"), T.fmatrix("ko")
    # NOTE: x and ko are assumed to be column vectors of experiments aka. row vectors of genes, 
    # hence no transposing of x and ko
    # make sure diagonal is zero
    _BX = UBX(x, thea.fill_diagonal(W, 0), I_TF(n_TF, n_PK), I_PK(n_TF, n_PK), ko, mask_w)
    l = loss(UE(x, _BX, ko), W, lasso, reg)
    update = gd.updates[descent](cost=l[0], params=W)
    # allow_input_downcast=True to allow the ko ints to be changed to float32 in the model
    return th.function(inputs=[ko, x], outputs=l, updates=update, allow_input_downcast=True)


def predict_function(n_TF, n_PK, W, mask_w=False):
    """
    theano function for calculating prediction of X at next time step.
    :param n_TF: int
    :param n_PK: int
    :param W: theano shared variable for weights. 
    Cannot be numpy array since we want this function to change when W is changed.
    :return: theano function
    """
    x, ko = T.fmatrix("x"), T.fmatrix("ko")
    _BX = UBX(x, W, I_TF(n_TF, n_PK), I_PK(n_TF, n_PK), ko, mask_w)
    x_pred = X(_BX, ko)
    # allow_input_downcast=True to allow the ko ints to be changed to float32 in the model
    return th.function(inputs=[ko, x], outputs=x_pred, allow_input_downcast=True)


if __name__ == '__main__':
    # instantiate a new Ebernet 
    main(Ebernet())

