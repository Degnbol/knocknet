#!/usr/bin/env python3
import numpy as np

"""
This script is for overall project configurations.
The default values written here will apply globally 
and can be changed for a specific run by importing this file and setting its variables.
"""

# print options level
precision = precision_default = 5
np.set_printoptions(precision=precision, linewidth=300, suppress=True)
# color and ascii formatting
color = color_default = True


# train
# the starting epoch, this can be changed to keep track of continued training
epoch = epoch_default = 0
epochs = epochs_default = 800
n_print_training = n_print_training_default = 50
lasso = lasso_default = 0.001
gradient_descent = gradient_descent_default = "adam"
max_cascade = max_cascade_default = 5
transcription_iterations = transcription_iterations_default = 5

# io
array_suffix = array_suffix_default = ".mat"
array_suffixes = array_suffixes_default = [".mat", ".csv", ".tsv", ".txt"]

