#!/usr/bin/env python3
from pathlib import Path
import re
import argparse
import numpy as np
from utilities import design as de
from degnlib.degnutil import (array_util as ar, input_output as io, path_util as pa, collection_util as co, 
                              argument_parsing as argue, read_write as rw, pandas_util as panda, sci_util as sci)
from degnlib import rocauc as ROC
from utilities.feature_prompt import feature_prompt
import configurations as config
from imshow import write as imshow_write, plot as imshow
import plot

"""
This script is a interactive access point for the project.
It should be possible to run anything else in the project through this.
This should both mean setting parameters for running other scripts in one line, 
as well as opening an input dialog program where settings can be set and viewed before running.
"""


class Knocknet:
    
    
    # static and constant
    prologue = "========= knocknet ========="
    epilogue = "============================"
    # for specific keys among the arrays that are trained or trainable weights
    weight_keys = []
    
    def __init__(self):
        self.function_names = []
        # main_function arrays that are read_arrays_function and used in the program, 
        # e.g. imported net and trained or untrained weight matrices
        self.arrays = {}

        # at init we don't assume interactive menu so we parse the command line call without adding menu items yet
        self.parser, _ = self.get_function_parser()
        self.args = self.parser.parse_args()
    

    def add_subparsers(self, addto, parsers):
        """
        Add parsers to a subparser group.
        The name of the parser is added to the list of word completions as well.
        :param addto: subparsers group to add the parser to.
        :param parsers: dict of the parsers to add. 
        The keys are used as the identifier when starting a subcommand for the parser
        :return: the subparser group for chaining
        """
        self.function_names.extend(parsers.keys())
        return argue.add_subparsers(addto, parsers)


    def get_function_parser(self, add_help=True):
        """
        The parser used from commandline to run major functions
        :param add_help: bool indicating if we add the -h/--help command
        :return: 
        """
        usage = None if add_help else argparse.SUPPRESS
        parser = argparse.ArgumentParser(
            add_help=add_help, usage=usage, fromfile_prefix_chars='@',
            description="This is the Knocknet project. "
                        "The overall goal is to create a network mimicking regulatory pathways using knockout net. "
                        "Other selftypes can also be used as priors. ")
        
        subparsers = self.add_subparsers(parser, self.get_commandline_functions())
        
        return parser, subparsers
    
    
    def get_commandline_functions(self):
        return {"config": self.get_config_parser(), "cmd": self.get_command_parser()}
    
    
    def get_menu_functions(self):
        """
        Get the parsers used within the program to run all menu functions and settings
        :return: dict
        """
        return {"help": self.get_help_parser(), "quit": self.get_quit_parser(), "bash": self.get_bash_parser(), 
                "py": self.get_python_parser(), "read": self.get_read_parser(), "write": self.get_write_parser(), 
                "array": self.get_array_parser(), "info": self.get_info_parser(), "plot": self.get_plot_parser(), 
                "imshow": self.get_imshow_parser(), "edge": self.get_edge_parser(), "node": self.get_node_parser(), 
                "pcor": self.get_partial_correlation_parser(), "eye": self.get_eye_parser(), "del": self.get_delete_parser(),
                "random": self.get_random_parser(), "rename": self.get_rename_parser(), "auc": self.get_auc_parser(),}


    def get_menu_parser(self):
        """
        Get the parser for interactive menu.
        :return: argparse.ArgumentParser
        """
        parser, function_parsers = self.get_function_parser(add_help=False)
        self.add_subparsers(function_parsers, self.get_menu_functions())
        return parser
    
    
    def update_menu_parser(self):
        """
        Update the parser to handle all the input commands that are allowed in interactive mode.
        """
        self.function_names = []
        self.parser = self.get_menu_parser()

    # parsers

    def get_help_parser(self):
        parser = argparse.ArgumentParser(add_help=False, description="Display help message.")
        parser.set_defaults(function=self.help_function)
        return parser


    def get_quit_parser(self):
        parser = argparse.ArgumentParser(add_help=False, description="Quit the program with a warning prompt.")
        parser.set_defaults(function=self.quit_function)
        return parser


    def get_command_parser(self):
        parser = argparse.ArgumentParser(add_help=False, description="Run commands in the program from file(s).")
        parser.set_defaults(function=self.command_function)
        parser.add_argument("file", type=argparse.FileType('r'), help="File with a commands written on each line.")
        parser.add_argument("cmd_args", nargs="*", default=[], help="Args given to the command file (used as ${@}, ${1}, ${2}, etc.).")
        return parser


    def get_bash_parser(self):
        parser = argparse.ArgumentParser(add_help=False, description="Run a bash command.")
        parser.add_argument("command", nargs="+",
                            help="the bash command to run. "
                                 "If it contains arguments with dashes, either use quotes or lead with -- .")
        parser.set_defaults(function=self.bash_function)
        return parser


    def get_python_parser(self):
        parser = argparse.ArgumentParser(add_help=False, description="Run a python command.")
        parser.set_defaults(function=self.python_function)
        parser.add_argument("command", nargs="+",
                            help="The python command to run.")
        parser.add_argument("-e", "--easy", action="store_true", 
                            help="Add easiness to writing your expression, e.g. all words are assumed to be array names.")
        parser.add_argument("-p", "--print", action="store_true",
                            help="Add print() around the command.")
        return parser


    def get_config_parser(self):
        parser = argparse.ArgumentParser(add_help=False, description="Get or set configuration value.")
        parser.add_argument("variable", nargs="?", 
                            help="the variable to change or variable to view. Ignore this to view all.")
        set_group = parser.add_mutually_exclusive_group()
        set_group.add_argument("value", nargs="?", help="the value to change the variable to.")
        set_group.add_argument("-reset", "--reset", action="store_true", help="Flag indicates that the variable is reset.")
        parser.set_defaults(function=self.config_function)
        return parser


    def get_read_parser(self):
        parser = argparse.ArgumentParser(add_help=False, description="Read array(s).")
        parser.add_argument("path", nargs="+",
                            help="The path(s) of the array(s) to read_arrays_function. "
                                 "Prefix of filename can also be used, e.g. leaving out the file suffix.")
        parser.add_argument("-name", "--name", nargs="*", default=[],
                            help="The naming to the array(s). "
                                 "If this argument is ignored, the name will be the stem of the path.")
        inc_or_3d = parser.add_mutually_exclusive_group()
        inc_or_3d.add_argument("-inc", "--increment", action="store_true", 
                            help="Read the most incremented version of the given name.")
        inc_or_3d.add_argument("-3d", "--3d", dest="all_incs", action="store_true",
                            help="Read all increments of the given name as each a layer of a 3D array.")
        parser.set_defaults(function=self.read_function)
        return parser


    def get_write_parser(self):
        parser = argparse.ArgumentParser(add_help=False,
                                         description="Write array(s). If file aready exists there is numbering added by default.")
        parser.set_defaults(function=self.write_function)
        argue.force(parser)
        argue.delimiter(parser)
        # append makes it possible to use append const with the cost that args.name will be a jagged array
        parser.add_argument("name", nargs="*", default=[], action="append", help="The name of the array(s) to write.")
        parser.add_argument("-path", "--path", nargs="*", default=[],
                            help="The path(s) to write the array(s) to. Ignore to give default array suffix.")
        parser.add_argument("-weights", "--weights", dest="name", action="append_const", const=self.weight_keys, default=[],
                            help="Write weights to file using default name.")
        
        return parser


    def get_array_parser(self):
        parser = argparse.ArgumentParser(add_help=False, 
                                         description="Show array info. If no arrays are chosen, list arrays.")
        argue.verbose(parser)
        selection_group = parser.add_mutually_exclusive_group()
        # append to allow for inheritance where other args add arrays to this
        selection_group.add_argument("name", nargs="*", default=[], action="append", help="Name of array(s) selected.")
        selection_group.add_argument("-a", "--all", action="store_true", help="Select all arrays.")
        
        parser.set_defaults(function=self.array_function)
        return parser


    def get_delete_parser(self):
        parser = argparse.ArgumentParser(add_help=False, description="Delete array(s).")
        selection_group = parser.add_mutually_exclusive_group()
        # append to allow for inheritance where other args add arrays to this
        selection_group.add_argument("name", nargs="*", default=[], action="append", help="Name of array(s) selected.")
        selection_group.add_argument("-a", "--all", action="store_true", help="Select all arrays.")

        parser.set_defaults(function=self.delete_function)
        return parser

    
    def get_info_parser(self):
        parser = argparse.ArgumentParser(add_help=False,
                                         description="Print info.")
        parser.set_defaults(function=self.info_function)

        return parser


    def get_plot_parser(self):
        parser = plot.get_parser(False)
        parser.set_defaults(function=self.plot_function)
        return parser


    def get_imshow_parser(self):
        parser = argparse.ArgumentParser(add_help=False, description="Plot a matrix.")
        parser.set_defaults(function=self.imshow_function)
        parser.add_argument("name", help="Name of the array to plot.")
        parser.add_argument("-path", "--path", metavar="filename",
                            help="Filename for the saved plot. Default is array name with ending .pdf.")
        parser.add_argument("-title", "--title", help="A title to add to the image")
        
        return parser

    
    def get_edge_parser(self):
        parser = argparse.ArgumentParser(add_help=False, 
                                         description="Use selected array(s) to create or add to an edge table. "
                                                     'The edge table is saved among the arrays with name "edges".')
        parser.set_defaults(function=self.edge_function)
        # append to allow for inheritance where other args add arrays to this
        parser.add_argument("name", nargs="*", default=[], action="append", help="Name of the array(s) to use.")
        parser.add_argument("-thres", "--threshold", type=float, default=0.001,
                            help="The threshold for ignoring weak edge connections.")

        return parser


    def get_node_parser(self):
        parser = argparse.ArgumentParser(add_help=False,
                                         description="Use selected array(s) to create or add to a node table. "
                                                     'The node table is saved among the arrays with name "nodes".')
        parser.set_defaults(function=self.node_function)
        # append to allow for inheritance where other args add arrays to this
        parser.add_argument("name", nargs="*", default=[], action="append", help="Name of the array(s) to use.")
        parser.add_argument("-ko", "--knockout", action="store_true", help="Add knockouts datapoints to the node table.")

        return parser

    
    def get_partial_correlation_parser(self):
        parser = argparse.ArgumentParser(add_help=False,
                                         description="Get partial correlation of a matrix of observations.")
        parser.set_defaults(function=self.partial_correlation_function)
        # append to allow for inheritance where other args add arrays to this
        parser.add_argument("name", help="Name of the array to use. "
                                         "By default each row is a variable and each column an observation.")
        parser.add_argument("-out", "--out", default="pcor", help='Name for result.')
        parser.add_argument("-colvar", "--column-variables", action="store_true", 
                            help='Set flag if each column is a variable and each row an observation of all variables.')

        return parser

    
    def get_eye_parser(self):
        parser = argparse.ArgumentParser(add_help=False,
                                         description="Create an identity matrix.")
        parser.set_defaults(function=self.eye_function)
        # append to allow for inheritance where other args add arrays to this
        parser.add_argument("name", help="Naming of the identity matrix.")
        parser.add_argument("shape", nargs="+", 
                            help="Shape of the matrix. E.g. one or two integers or a name of another array which shape will be used.")

        return parser
    
    
    def get_random_parser(self):
        parser = argparse.ArgumentParser(add_help=False,
                                         description="Create a random matrix. Gaussian by default.")
        parser.set_defaults(function=self.random_function)
        # append to allow for inheritance where other args add arrays to this
        parser.add_argument("name", help="Naming of the random matrix.")
        parser.add_argument("shape", nargs="+", 
                            help="Shape of the matrix. E.g. one or two integers or a name of another array which shape will be used.")
        sparsity_parser = parser.add_mutually_exclusive_group()
        sparsity_parser.add_argument("-sparse", "--sparsity", type=float, default=0., help="Probability that a value is left as zero.")
        sparsity_parser.add_argument("-edge", "--max-edges", type=int, 
                                     help="Number of nonzero entries in each column will be between 1 and this argument.")
        parser.add_argument("-unif", "--uniform", nargs="*", type=int, help="Use uniform distribution with given lower and upper limit.")
        parser.add_argument("-std", "--std", default=1., type=float, help="Standard deviation for the noise to add.")

        return parser

    
    def get_rename_parser(self):
        parser = argparse.ArgumentParser(add_help=False,
                                         description="Rename an array.")
        parser.set_defaults(function=self.rename_function)
        parser.add_argument("name", help="Original name of array.")
        parser.add_argument("out", help="New name of array.")
        return parser


    def get_auc_parser(self):
        parser = argparse.ArgumentParser(add_help=False,
                                         description="Get AUC of two arrays.")
        parser.set_defaults(function=self.auc_function)
        parser.add_argument("y_true", help="Array key for true labels.")
        parser.add_argument("y_score", help="Array key for score values.")
        parser.add_argument("-sign", "--sign", action="store_true", help="Get the pos, neg, macro and micro AUCs.")
        return parser


    # functions

    def help_function(self):
        """
        Display help message.
        :return: None
        """
        print("Run one of the functions:")
        print(self.function_names)
        print("Follow by -h/--help to get help for each function.")


    def quit_function(self):
        """
        Code to run if quitting menu is chosen.
        :return: False if quitting, True otherwise
        """
        return False
        # return not io.confirm("Quit program (y/n)? ")


    def command_function(self):
        """
        Run commands read from file
        :return: False if to exit program
        """
        cmd_args = self.args.cmd_args
        # allow all the commands for interactive mode in case we are running this in command line
        self.update_menu_parser()
        with self.args.file as file:
            for line in file:
                line = line.strip()
                # replace arg variables with their given value if written as ${1}
                for i, arg in enumerate(cmd_args):
                    line = line.replace("${" + str(i+1) + "}", arg)
                line = line.replace("${@}", " ".join(cmd_args))
                print("> " + line)
                if self.run_input(line) is False: return False


    def bash_function(self):
        """Run a bash command. """
        # collect the command to one, it might be split in pieces and replace newlines
        command = " ".join(self.args.command)
        io.process(command)


    def python_function(self):
        """Run a python command. """
        # collect the command to one, it might be split in pieces
        command = " ".join(self.args.command)
        
        if self.args.print: command = "print(" + command + ")"
        
        if self.args.easy:
            # make sure commands are ready for a dataframe or array
            for f in ["sum", "mean", "std", "min", "max", "median"]:
                # if not preceded by a dot
                command = re.sub(r"(?!\\.)" + f, r"panda." + f, command)
            for f in ["corr", "cov"]:
                # if not preceded by a dot
                command = re.sub(r"(?!\\.)" + f + '\(', r"ar." + f + '(', command)
            
            # if not assignment takes place and we only have 1 command we print
            if "=" not in command:
                if "\n" not in command and ";" not in command:
                    command = "print(" + command + ")"
            
            io.log.debug("Running command: " + command)
        
        local = locals()
        # add all arrays to local memory
        local = {**local, **self.arrays}
        try: exec(command, globals(), local)
        except Exception as exception: print("There was an error:", exception)
        else:
            # add all changes in local array to kept arrays
            for key in self.arrays: self.arrays[key] = local[key]


    def config_function(self):
        if self.args.reset: self._reset_config()
        elif self.args.value is None: self._get_config()
        else: self._set_config()


    def _reset_config(self):
        """
        Reset one, some or all settings.
        :return: 
        """
        # if no variable is chosen, all are found and then reset
        if not self.args.variable: variables = dir(config)
        else: variables = [self.args.variable]
        for variable in variables:
            # ignore builtins
            if not variable.startswith("__"):
                try: setattr(config, variable, getattr(config, variable + "_default"))
                # if there is no default value found we ignore it
                except AttributeError: pass

        self.update_menu_parser()


    def _get_config(self):
        """Get the value of a variable. """
        if not self.args.variable: variables = dir(config)
        else: variables = [self.args.variable]
        for variable in variables:
            # ignore builtins
            if not variable.startswith("__"):
                attr = getattr(config, variable)
                value = str(attr)
                # ignore classes, modules, function, etc.
                if not (value.startswith("<") and value.endswith(">")):
                    if isinstance(attr, str): value = '"' + value + '"'
                    print(variable, "=", value)


    def _set_config(self):
        """Get the value of a variable. """
        if not self.args.variable:
            raise argparse.ArgumentTypeError("Only one config can be set at a time.")
        setattr(config, self.args.variable, eval(self.args.value))
        self.update_menu_parser()


    def read_function(self):
        """
        Read an array and assign it a name in a general array dict.
        :return: 
        """
        if 0 != len(self.args.name) != len(self.args.path):
            raise argparse.ArgumentError("If given name(s), as many should be given as the path argument.")

        # find file in cwd using path input as prefix
        paths = [pa.shortest_match_prefix(p) for p in self.args.path]
        for i in range(len(paths)):
            # if we found multiple matches, we choose filter for typical suffixes
            if len(paths[i]) > 1: paths[i] = pa.match_suffix(config.array_suffixes, paths[i])
            # raise errors if we still don't have a perfect match
            if len(paths[i]) > 1: raise ValueError("Multiple files matched the given name")
            if len(paths[i]) == 0: raise FileNotFoundError()
            paths[i] = paths[i][0]

        if not self.args.name: self.args.name = [Path(p).stem for p in paths]
        
        if self.args.increment:
            paths = [pa.get_highest_increment(p) for p in paths]
        
        for path, name in zip(paths, self.args.name):
            if not self.args.all_incs:
                io.log.info("reading array {} from path {}".format(name, path))
                self.arrays[name] = rw.read_array(path)
            else:
                paths = pa.get_increments(path)
                io.log.info("reading array {} from paths {}".format(name, paths))
                self.arrays[name] = np.stack([rw.read_array(p) for p in paths])
                assert self.arrays[name].ndim == 3
    

    def write_function(self):
        """
        Write array(s) to path(s).
        :return: 
        """
        self.args.name = co.unjag_list(self.args.name)
        
        if 0 != len(self.args.path) != len(self.args.name):
            if len(self.args.path) != len(self.args.name) - len(self.weight_keys):
                raise argparse.ArgumentTypeError("If given path(s), as many should be given as the name argument.")
        
        # use default suffix on variable names following the paths that were already given
        # NOT INPLACE !!! that would change the parsers mutable [] and change the default value.
        self.args.path = self.args.path + [n + config.array_suffix for n in self.args.name[len(self.args.path):]]
        
        # check the names given have extensions on them
        for i, name in enumerate(self.args.name):
            name = Path(name)
            if name.suffix:
                self.args.path[i] = self.args.name[i]
                self.args.name[i] = name.stem

        # increment all paths with the same number
        if not self.args.force and pa.exists(self.args.path):
            self.args.path = pa.increment(self.args.path)

        for name, path in zip(self.args.name, self.args.path):
            if not name in self.arrays:
                print("array {} not found".format(name))
                continue
            io.log.info("writing array {} to {}".format(name, path))
            rw.write_array(self.arrays[name], path, sep=self.args.sep)
    
    
    def array_function(self):
        """ Show info for array(s) or delete them depending on arguments. """
        if self.args.all: self.args.name = list(self.arrays.keys())
        else: self.args.name = co.unjag_list(self.args.name)
        
        if len(self.args.name) == 0:
            return print("arrays:", list(self.arrays.keys()))
        
        for name in self.args.name:
            try: array = self.arrays[name]
            except KeyError:
                print("array name not recognized")
                return
            # clarify name if there are more than over array to summarize
            if len(self.args.name) > 1: print("array:", name)
            print("shape =", np.shape(array))
            
            # get statistics if it is an array of numbers
            try:
                mini, maxi, mean, std, median = panda.min(array), panda.max(array), panda.mean(array), \
                                                panda.std(array), panda.median(array)
            except TypeError: pass # must be an array of strings
            else:
                try: print("min = {:d}\tmax = {:d}".format(mini, maxi))
                except ValueError: print("min = {:.4f}\tmax = {:.4f}".format(mini, maxi))
                print("mean = {:.4f}\tstd = {:.4f}\tmedian = {:.4f}".format(mean, std, median))
            if self.args.verbose: print(de.color_array(array))


    def delete_function(self):
        """ Show info for array(s) or delete them depending on arguments. """
        if self.args.all:
            self.arrays = {}
            io.log.info("all arrays removed from memory")
        else:
            self.args.name = co.unjag_list(self.args.name)
            for key in self.args.name:
                del self.arrays[key]
                io.log.info("array {} removed from memory".format(key))
    
    
    def info_function(self):
        print("starting epoch =", config.epoch)


    def plot_function(self):
        self.args.tables = [self.arrays[t] for t in self.args.tables]
        plot.main(self.args)


    def imshow_function(self):
        if not self.args.path: self.args.path = Path(self.args.name).with_suffix(".pdf")
        try: array = self.arrays[Path(self.args.name).stem]
        except KeyError: return print("array name not recognized")
        imshow(array, self.args.title)
        imshow_write(self.args.path)
        io.log.info("written to", self.args.path)
    
    
    def edge_function(self, args=None):
        """
        Make an edge table or add to existing one
        :return: None
        """
        if args is None: args = self.args
        args.name = co.unjag_list(args.name)
        
        if len(args.name) == 0:
            return print("no array selected")
        if not all(k in self.arrays for k in args.name):
            return print("not all selected arrays found")
        if not all(hasattr(self.arrays[k], "columns") for k in args.name):
            return print("not all selected arrays have named axis")
        
        key = "edges"
        edges = panda.arrays_to_edges({k: self.arrays[k] for k in args.name}, args.threshold, "type")
        if key in self.arrays: edges = panda.concat_pandas((self.arrays[key], edges))
        self.arrays[key] = edges


    def node_function(self, args=None):
        """
        Make a node table or add to existing one
        :return: None
        """
        if args is None: args = self.args
        args.name = co.unjag_list(args.name)
        
        if len(args.name) == 0:
            return print("no array selected")
        if not all(k in self.arrays for k in args.name):
            return print("not all selected arrays found")
        if not all(hasattr(self.arrays[k], "columns") for k in args.name):
            return print("not all selected arrays have named axis")
        
        # we cut off all after the first "_" so e.g. using key "pk_ko" we get the type "pk" 
        arrays = {n.split("_")[0]: self.arrays[n].transpose() for n in args.name}
        # if we don't want knockout data we are really just removing all contents of the arrays and only keeping the index
        if not args.knockout: arrays = {k: arrays[k].drop(a.columns, axis=1) for k, a in arrays.items()}
        # concat and add type column
        nodes = panda.concat_pandas(arrays, group="type")
        nodes.index.name = "name"
        
        key = "nodes"
        # if already exists, we append
        if key in self.arrays: nodes = panda.concat_pandas((self.arrays[key], nodes))
        self.arrays[key] = nodes
    
    
    def partial_correlation_function(self):
        """
        Make partial correlation for matrix of observations.
        :return: 
        """
        arr = self.arrays[self.args.name]
        if self.args.column_variables: arr = arr.T
        # make sure it is float64 so inverse is calculated correctly
        self.arrays[self.args.out] = ar.partial_corr(arr.astype('float64', copy=False))
    
    
    def eye_function(self):
        """
        Make an identity matrix.
        :return: 
        """
        self.parse_shape()
        self.arrays[self.args.name] = np.eye(*self.args.shape)

    
    def random_function(self):
        """
        Make a random matrix.
        :return: 
        """
        self.parse_shape()
        shape = self.args.shape
        if len(shape) == 1: shape *= 2
        if self.args.uniform is None: arr = sci.normal_trunc(size=shape, std=self.args.std)
        else:
            if not self.args.uniform: self.args.uniform = [0, 1]
            arr = np.random.uniform(*self.args.uniform, size=shape)
        
        # remove some values with a sparsity mask
        if self.args.sparsity:
            p = [self.args.sparsity, 1 - self.args.sparsity]
            arr *= np.random.choice([0, 1], p=p, size=shape)
        elif self.args.max_edges:
            arr *= ar.random_mask(self.args.max_edges, arr.shape, True)
        self.arrays[self.args.name] = arr


    def rename_function(self):
        self.arrays[self.args.out] = self.arrays[self.args.name]
        del self.arrays[self.args.name]
    
    
    def auc_function(self):
        y_true, y_score = self.arrays[self.args.y_true], self.arrays[self.args.y_score]
        if not self.args.sign: print(ROC.ROCAUC(y_true, y_score))
        else:
            fprs, tprs = ROC.ROCs(y_true, y_score)
            for fpr, tpr in zip(fprs, tprs): print(ROC.AUC(fpr, tpr))
    
    # methods

    def parse_shape(self):
        if len(self.args.shape) > 2: raise argparse.ArgumentTypeError("Shape can max be 2 values")
        for i, s in enumerate(self.args.shape):
            try:
                self.args.shape[i] = int(s)
            except ValueError:
                try:
                    self.args.shape[i] = self.arrays[s].shape[i]
                except KeyError:
                    raise argparse.ArgumentTypeError("Could not understand shape argument")

    
    def get_complete_words(self):
        """
        Define words to suggest for completion
        :return: list of string words to suggest for word completion
        """
        words = [str(w.relative_to(Path.cwd())) for w in Path.cwd().iterdir() if pa.has_suffix(w, config.array_suffixes)]
        return words + self.function_names

    
    def run_input(self, inp):
        """
        Run a text input command
        :return: False if we should exit program
        """
        # allow empty input and comments
        if not inp or inp.startswith("#"): return
        try:
            self.args = argue.parse_args(self.parser, inp)
        # don't quit entire program when displaying help messages
        except SystemExit: return
        except Exception:
            io.log.error("parsing failed", exc_info=True)
            return
        # run function and quit program on return value False
        try:
            if self.args.function() is False: return False
        except KeyboardInterrupt: io.print_overwrite("Interrupted\n")
        # display errors without quitting entire program
        except Exception: io.log.error("command failed", exc_info=True)
    

    # main_function

    def interactive(self):
        """
        Run the main_function interactive menu
        :return: 
        """
        print(self.prologue)

        self.update_menu_parser()
        complete_words = self.get_complete_words()

        while True:
            try: inp = feature_prompt(complete_words)
            # allow for interrupting while writing a command
            except KeyboardInterrupt: io.print_overwrite("Interrupted\n")
            else:
                if self.run_input(inp) is False: break

        print(self.epilogue)


def main(network):
    """
    Main access to running a network either in single commandline or interactively.
    :param network: an instance of Knocknet class or a child of it.
    :return: 
    """
    # see if the commandline was given a function to run
    try: function = network.args.function
    # if it does not have the function attribute, it means the program is meant to simply start in interactive mode
    except AttributeError: network.interactive()
    # run the function that was given on commandline
    else: function()


if __name__ == '__main__':
    # instantiate a new default network
    from gnwnet import Gnwnet
    main(Gnwnet())
    
    

