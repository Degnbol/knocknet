for folder in insil*; do
cd $folder
knocknet cmd ../synth.cmd.sh
gene2B.R gene.mat B.mat
knocknet cmd ../B2W.cmd.sh
cd -
done
cat */edges.mat > edges.mat
cat */weight.mat > weight.mat
rocauc.py edges.mat weight.mat -out rocplot_r.png > auc_r.log
