for folder in insil*; do
cd $folder
knocknet cmd ../synth.cmd.sh
knocknet cmd ../train.cmd.sh
cd -
done
cat */edges.mat > edges.mat
cat */weight.mat > weight.mat
rocauc.py edges.mat weight.mat -out rocplot.png > auc.log
