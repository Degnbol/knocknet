read gene edges B
eye ko 10
init
define-network -lasso 0.00001 -reg l1 -n-pk 3 -n-tf 7
B2W -ep 12000 -prints 5
predict
python -e "weight = weight * 10"
array -v edges weight
auc edges weight
write weight -f
