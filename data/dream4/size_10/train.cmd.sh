read gene edges
eye ko 10
init
define-network -lasso 0.01 -reg l1 -n-pk 3 -n-tf 7
train -ep 30000 -prints 5
predict
python -e "weight = weight * 10"
array -v edges weight
auc edges weight
write weight -f
