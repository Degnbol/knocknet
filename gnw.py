#!/usr/bin/env python3
# coding=utf-8
import random
import numpy as np
import scipy.integrate as spi
from degnlib.degnutil import array_util as ar, sci_util as sci


class RegulatoryModule:
    
    @staticmethod
    def random_k(size):
        """
        :param size: int number of inputs so n_activators + n_deactivators
        :return: 
        """
        return np.random.uniform(0.01, 1, size=size)

    @staticmethod
    def random_n(size):
        """
        :param size: :param size: int number of inputs so n_activators + n_deactivators
        :return: 
        """
        return sci.normal_trunc(1, 10, 2, 2, size=size)
    
    @staticmethod
    def random_inhibitor(n_activators, n_deactivators):
        """
        if a module is overall an enhancer or inhibitor based on a number of activators and deactivators
        :return: bool
        """
        if n_activators == n_deactivators: return random.choice([False, True])
        return n_activators < n_deactivators
    
    def __init__(self, activators, deactivators, k=None, n=None, binds_as_complex=None, is_inhibitor=None):
        """
        :param activators: list of indexes referring to activators in the overall list of proteins 
        :param deactivators: 
        :param k: Dissociation constants k
        :param n: Hill coefficients for the regulators (activators and deactivators)
        :param binds_as_complex: 
        """
        self.n_activators = len(activators)
        self.n_deactivators = len(deactivators)
        self.inputs = list(activators) + list(deactivators)
        self.k = k
        self.n = n
        self.binds_as_complex = binds_as_complex
        self.is_inhibitor = is_inhibitor
    
    def initialize_random(self):
        """
        If a setting is not set for this module it will here be set randomly.
        """
        if self.k is None:
            self.k = self.random_k(len(self.inputs))
        if self.n is None:
            self.n = self.random_n(len(self.inputs))
        if self.binds_as_complex is None:
            self.binds_as_complex = random.choice([False, True])
        if self.is_inhibitor is None:
            self.is_inhibitor = self.random_inhibitor(self.n_activators, self.n_deactivators)
            # if we turn out to be an inhibitor, we reverse all inputs
            # so what was called a repressor before will still be a repressor of the gene by being an activator of this
            # inhibiting regulatory unit
            if self.is_inhibitor:
                # swap activators and deactivators
                self.inputs = self.inputs[self.n_activators:] + self.inputs[:self.n_activators]
                self.n_activators, self.n_deactivators = self.n_deactivators, self.n_activators
                
    
    def mean_activation(self, phi):
        """
        Return mean activation by this regulatory module given its inputs that are in phi.
        :param phi: 1D array of concentrations of all active TF proteins
        :return: float mean regulator activation of its target gene
        """
        chi = (phi[self.inputs] / self.k) ** self.n
        prod_active = np.prod(chi[:self.n_activators])
        if self.binds_as_complex:
            # if it is a complex, there are three states of the module,
            # either the activated complex is bound, the deactivated complex is bound, or it is not bound

            # activated complex bound
            denominator = 1 + prod_active
            if self.n_deactivators > 0:
                denominator += np.prod(chi)
        else:
            denominator = np.prod(1 + chi)
            
        return prod_active / denominator



class Gene:
    
    _weak_activation = 0.25

    @staticmethod
    def random_modules(input_nodes, input_edges):
        """
        :param input_nodes: int 1D array of indexes in the overall list of proteins for the TFs controlling this gene
        :param input_edges: int 1D array of the edge value for the inputs, if negative it has a repressing effect
        :return: list of regulatory module instances
        """
        max_modules = n_inputs = len(input_nodes)
        # distribute the nodes randomly into modules where some might have 1, some might have multiple nodes
        module_indexes = [[] for _ in range(max_modules)]
        for i in range(n_inputs):
            module_indexes[random.randint(0, n_inputs-1)].append(i)
        # don't make empty modules
        module_indexes = list(filter(None, module_indexes))
        
        modules = []
        for indexes in module_indexes:
            nodes, edges = input_nodes[indexes], input_edges[indexes]
            # if there was by accident any edges with value 0 they are filtered out by the strict less and greater than
            module = RegulatoryModule(nodes[edges > 0], nodes[edges < 0])
            module.initialize_random()
            modules.append(module)
        
        return modules

    @staticmethod
    def random_alpha(modules):
        """
        :param modules: list of modules for which to make random alpha
        :return: 1D array of length 2^n_modules 
        """
        n_modules = len(modules)
        # the effect on alpha_0 of each module individually
        # set the difference in gene activation due to any module alone
        delta_activation = Gene._random_delta_activation(n_modules)
        
        is_inhibitors = np.asarray([m.is_inhibitor for m in modules])
        n_inhibitors = sum(is_inhibitors)
        
        # Set alpha0, the basal transcription rate
        if n_modules == 0:  # no inputs means alpha is the basal level, alpha0
            return np.asarray([1])
        # only inhibitors
        elif n_modules == n_inhibitors:
            alpha0 = 1
        # only activators
        elif n_inhibitors == 0:
            alpha0 = Gene._random_low_basal_rate().item()
        # there are enhancers and inhibitors
        else:
            alpha0 = Gene._random_medium_basal_rate().item()

        # Compute the max positive and negative difference in gene activation, 
        # i.e., when all enhancers / inhibitors are active
        delta_activation[is_inhibitors] *= -1
        delta_enhancer = delta_activation[~is_inhibitors]
        delta_inhibitor = delta_activation[is_inhibitors]
        
        # make sure that the activation goes at least up to 1 in the maximally activated state 
        # if there is at least one activator
        if n_modules > n_inhibitors and alpha0 + sum(delta_enhancer) < 1:
            # increase the smallest delta_activation so that: alpha0 + sum(delta_enhancer) = 1
            delta_enhancer[np.argmin(delta_enhancer)] += 1 - alpha0 - sum(delta_enhancer)
            # update delta_activation to the change
            delta_activation[~is_inhibitors] = delta_enhancer

        # make sure that the activation falls within [0 weakActivation] in the maximally repressed state 
        # if there is a at least one repressor
        if n_inhibitors and alpha0 + sum(delta_inhibitor) > Gene._weak_activation:
            # increase the weakest delta_activation so that: (alpha0 + sum(delta_inhibitor)) in [0 weakActivation]
            delta_inhibitor[np.argmax(delta_inhibitor)] += - alpha0 - sum(delta_inhibitor) + Gene._random_low_basal_rate()
            # update delta_activation to the change
            delta_activation[is_inhibitors] = delta_inhibitor

        # Set the alpha for all possible states
        # State s is interpreted as a binary number, bit k indicates whether module k is active (True) or inactive (False) in this state. 
        # State 0 (alpha0) has already been set
        module_index = (ar.binary(i, n_modules) for i in range(2 ** n_modules))
        alpha = np.asarray([alpha0 + np.sum(delta_activation[idx]) for idx in module_index])
        return np.clip(alpha, 0, 1)

    @staticmethod
    def _random_delta_activation(size):
        return sci.normal_trunc(Gene._weak_activation, 1, size=size)

    @staticmethod
    def _random_low_basal_rate():
        return sci.normal_trunc(0, Gene._weak_activation, 0, 0.05)

    @staticmethod
    def _random_medium_basal_rate():
        return sci.normal_trunc(Gene._weak_activation, 1 - Gene._weak_activation)


    def __init__(self, modules, alpha):
        """
        Create new gene.
        :param modules: list of regulatory modules controlling this gene.
        :param alpha: 
        """
        self.modules = modules
        self.alpha = alpha


    def f(self, phi):
        """
        :param phi: 1D array of active part of all TF proteins
        :return: 
        """
        # if we are the special case of gene where there is no input proteins the transcription is always at its max
        if len(self.modules) == 0: return 1
        # get mean activation of each regulatory module
        mean_activation = np.asarray([module.mean_activation(phi) for module in self.modules])
        # get P{state} for all states, each state is a unique combination of modules
        module_index = (ar.binary(i, len(self.modules)) for i in range(2 ** len(self.modules)))
        p = [mean_activation[idx].prod() * (1 - mean_activation[~idx]).prod() for idx in module_index]
        return sum(self.alpha * np.asarray(p))


    def edges(self, n_tf):
        """
        Get the edge strength for each TF input onto this gene.
        :param n_tf: size of the output vector, number of TFs
        :return: 1D array with edge values inserted at appropriate indexes and zeros for the rest.
        """
        out = np.zeros(n_tf)
        basal = self.f(np.zeros(n_tf))
        for module in self.modules:
            for inp in module.inputs:
                out[inp] = self.f(ar.invwhere(inp, n_tf, dtype=float)) - basal
        return out



class Network:
    
    min_rpa = 1e-8

    @staticmethod
    def random_genes(adjacency_matrix):
        """
        Return a list of gene objects created randomly and connected by TF edges specified in the given weight matrix.
        :param adjacency_matrix: weight matrix showing the edges from TF protein to all proteins (both TF and PK).
        The index [i,j] indicates the edge from TF j to protein i. 
        Only the sign is used, -1 for repressor, 0 for no influence, 1 for activator.
        This means if both edges with TF and PK as source are in the same weight matrix, there should be performed indexing before use.
        :return: list of gene instances
        """
        genes = []
        for row in adjacency_matrix:
            nodes = np.nonzero(row != 0)[0]
            modules = Gene.random_modules(nodes, row[nodes])
            genes.append(Gene(modules, Gene.random_alpha(modules)))
        return genes

    @staticmethod
    def random_half_life(size):
        return sci.normal_trunc(5, 50, size=size)

    @staticmethod
    def random_decay_rate(size):
        return np.log(2) / Network.random_half_life(size)
    
    
    def __init__(self, genes, W):
        """
        Create a new gene network for simulation of transcription, translation and phosphorylation (or other activation).
        :param genes: list of Gene instances. 
        :param W: 2D array weight describing the edges from PK to all proteins. 
        All values in W are positive. 
        They are the rate of phosphorylation mediated by the kinase for a specific receiver.
        """
        self.genes = genes
        self.W = W
        self.n_gene = len(genes)
        self.n_pk = W.shape[1]
        self.n_tf = self.n_gene - self.n_pk
        assert W.shape == (self.n_gene, self.n_pk)
        self.max_transcription, self.max_translation, self.mRNA_degradation, self.protein_degradation, \
            self.phosphorylation_degradation = [None for _ in range(5)]
        
    
    def set_random_rates(self):
        """
        Set random half-lives for this gene's products. In the non-dimensionalized
        model, max_ = delta_ and maxTranslation_ = deltaProtein_. We have exponential
        decay, the half-life and the decay rate are thus related by:
        t_1/2 = ln(2) / delta
        delta = ln(2) / (t_1/2)
        """
        self.max_transcription = self.mRNA_degradation = self.random_decay_rate(self.n_gene)
        self.max_translation = self.protein_degradation = self.random_decay_rate(self.n_gene)
        self.phosphorylation_degradation = self.random_decay_rate(self.n_gene)

    
    def initial_a(self):
        """
        Get the initial values for a, the phosphorylated part of p.
        a is needed for setting initial r and r is needed for setting initial p.
        :return: 
        """
        return np.repeat(.5, self.n_gene)
    

    def initial_r(self, phi_tf):
        """
        Get initial values for mRNA.
        It is estimated as the concentration of the mRNA without regulation.
        0 = dr/dt = m*f(a) -delta*r_i  =>  r_i = m*f(a) / delta
        where p=0
        :param phi_tf: 1D array with the levels of phosphorylated TF at initial conditions.
        :return: r at initial condition
        """
        return self.max_transcription * self._f(phi_tf) / self.mRNA_degradation
    
    
    def inital_p(self, r):
        """
        Get initial values for protein.
        It is estimated as the concentration of the proteins without regulation.
        0 = mTranslation*r_i - deltaProt*p_i  =>  p_i = mTranslation*r_i / deltaProt
        :param r: mRNA at initial conditions
        :return: p (protein) at initial condition
        """
        return self.max_translation * r / self.protein_degradation

    
    def initial_rpa(self, mask=None):
        """
        Get initial values for r, p, a as a single vector with length 3 * n_gene
        :param mask: optional list of the nodes with True for the nodes present and False for the nodes removed.
        If mask is 2D with shape n,m it contains m separate masks and returns m separate initial conditions. 
        :return: 1D array or 2D array if 2D mask is given. Each column is an experiment
        """
        if mask is None:
            a = self.initial_a()
            r = self.initial_r(a[:self.n_tf]) # use a in place of phi
            p = self.inital_p(r)
            return np.concatenate((r, p, a))
        
        if mask.ndim == 1:
            a = self.initial_a()
            a[~mask] = 0
            r = self.initial_r(a[:self.n_tf])
            r[~mask] = 0
            p = self.inital_p(r)
            p[~mask] = 0
            return np.concatenate((r, p, a))

        if mask.ndim == 2:
            out = []
            for _mask in mask.T:
                a = self.initial_a()
                a[~_mask] = 0
                r = self.initial_r(a[:self.n_tf])
                r[~_mask] = 0
                p = self.inital_p(r)
                p[~_mask] = 0
                out.append(np.concatenate((r, p, a))[np.newaxis])
            return np.concatenate(out).T
        

    def _f(self, phi):
        return np.asarray([gene.f(phi) for gene in self.genes])

    def drdt(self, phi, r):
        """
        Return the differential r wrt. t to get a small change in r.
        :param phi: phosphorylated concentration of TFs
        :param r: all mRNA levels
        :return: 1D array dr/dt
        """
        return self.max_transcription * self._f(phi) - self.mRNA_degradation * r

    def dpdt(self, r, p):
        """
        Return the differential p wrt. t.
        :param r: 1D array mRNA amounts
        :param p: 1D array protein amounts
        :return: 1D array dp / dt
        """
        return self.max_translation * r - self.protein_degradation * p

    def dadt(self, p, a, dpdt):
        """
        :param p: 1D array the overall amount of protein p = (phi + phi_tilde)
        :param a: 1D array the fraction of proteins phosphorylated
        :param dpdt: 1D array
        :return: 1D array d a / dt
        """
        return (self.dphidt(a*p, p) - dpdt * a) / p

    def dphidt(self, phi, p):
        """
        The function describing how phi is affected by other phis (phosphorylated proteins) and y (all proteins in general).
        It is sort of a counterpart to the f function.
        :param phi: 1D array of all phosphorylated kinases/phosphatases
        :param p: 1D array of all kinases/phosphatases
        :return: 
        """
        # assuming phi is sorted with TF first and PK after
        return (self.W @ phi[self.n_tf:]) * (p - phi) - self.phosphorylation_degradation * phi

    def drpadt(self, rpa, _):
        """
        Return all derivatives as a single vector given all variables as a single vector.
        :param rpa: r, p, a as single vector
        :param _: ignored parameter that ODE solvers uses for time if the derivatives were functions of time.
        :return: dr/dt, dp/dt, d a/dt as single vector
        """
        rpa[rpa < self.min_rpa] = self.min_rpa
        r, p, a = rpa[:self.n_gene], rpa[self.n_gene:2*self.n_gene], rpa[2*self.n_gene:]
        drdt, dpdt = self.drdt(a*p, r), self.dpdt(r, p)
        return np.concatenate((drdt, dpdt, self.dadt(p, a, dpdt)))
    
    
    def rpa(self, t, initial_rpa=None):
        """
        Get r, p, a as single vector at time(s) t found using scipy.integrate.odeint.
        :param t: number or iterable of numbers for the time points
        :param initial_rpa: optional initial conditions to set for r, p, a.
        If not given they will be set automatically which will have random variation each time.
        with True indicating a gene is transcribed and False otherwise.
        :return: r, p, a as single vector
        """
        if initial_rpa is None: initial_rpa = self.initial_rpa()
        _rpa = spi.odeint(self.drpadt, initial_rpa, t)
        assert not np.any(np.isnan(_rpa)), "NaNs produced"
        _rpa[_rpa < self.min_rpa] = self.min_rpa
        return _rpa

    
    def rpa_masked(self, t, transcription_mask, initial_rpa=None):
        """
        Get r, p, a as single vector at time(s) t found using scipy.integrate.odeint.
        :param t: number or iterable of numbers for the time points.
        :param transcription_mask: 2D array where each column is a mask for transcription in an experiment
        with True indicating a gene is transcribed and False otherwise.
        :param initial_rpa: optional initial conditions to set for r, p, a.
        If not given they will be set automatically which will have random variation each time.
        It can be 2D to indicate different initial conditions along axis=0.
        :return: 3D array with first axis mask, second axis time, and third axis node index 
        for vectors of r, p, a in that order.
        """
        if initial_rpa is None: initial_rpa = self.initial_rpa()
        wildtype_transcription = self.max_transcription
        rpa = []
        for i, mask_row in enumerate(transcription_mask.T):
            self.max_transcription = wildtype_transcription.copy()
            self.max_transcription[~mask_row] = 0
            if initial_rpa.ndim == 2:
                rpa.append(self.rpa(t, initial_rpa[:, i]))
            else:
                rpa.append(self.rpa(t, initial_rpa))
        # reset to wildtype
        self.max_transcription = wildtype_transcription
        return np.asarray(rpa)
    
    
    @ staticmethod
    def r_p_a(rpa):
        """
        Given an array containing r, p, a concatenated along the last axis 
        split it to get arrays with r, p, phi separately
        :param rpa: array with r, p, phi along the last axis, assumed to be in equal amounts.
        :return: tuple of 3 arrays of same shape except the last dimension that will be a third of before.
        """
        n = rpa.shape[-1] // 3
        return rpa[..., :n], rpa[..., n:2 * n], rpa[..., 2 * n:]
    
    
    def tf_edges(self):
        """
        Get an adjacency matrix from the network with its TF->gene edges as signed floats
        :return: 2D array where the values in row i represent all the edges onto gene i
        """
        return np.asarray([gene.edges(self.n_tf) for gene in self.genes])
    
    
    def pk_edges(self):
        """
        Get an adjacency matrix for the network with its PK->gene edges as signed floats
        :return: 2D array where the values in row i represent all the PK->protein i edges
        """
        return self.W
    
    
    def edges(self):
        return np.concatenate((self.tf_edges(), self.pk_edges()), axis=1)





def rpa_wt_ko(network, t, mask):
    """
    Get a tuple with xya values for wildtype and a range of knockout experiments at some time points.
    The initial conditions are made randomly.
    :param network: instance of Network
    :param t: range/list of time points
    :param mask: array masking to indicate nodes kept in each experiment
    :return: (xya_wt, xya_masked)
    """
    return network.rpa(t, network.initial_rpa()), \
           network.rpa_masked(t, mask, network.initial_rpa(mask))



def synthesize(adjacency, n_pk, knockout=None, thres=0.005, tries=10):
    """
    Synthesize logFC gene expression reads for a random network generated based on edge signs.
    :param adjacency: adjacency matrix with TF columns and PK columns. 
    TF edges are -1, 0, or 1 while PK edges are 0, 1
    :param n_pk: optional int number of columns toward right are for PK.
    :param knockout: optional logical knockout indexing matrix 
    indicating genes knockout with True in the first column for the first experiment and so on.
    By default each gene is KOed once by itself.
    :param thres: optional threshold for accepting convergence
    :param tries: number of times to try to randomize network before giving up in case of no convergence.
    :return: approximate edge values, logFC mRNA reads, and a class instance for replication purposes
    """
    n_gene = len(adjacency)
    if knockout is None: mask = ~np.eye(n_gene, dtype=bool)
    else: mask = ~knockout.astype(bool)
    
    tf_adj = adjacency[:, :-n_pk]
    pk_adj = adjacency[:, -n_pk:]
    
    for _ in range(tries):
        net = Network(Network.random_genes(tf_adj), pk_adj)
        net.set_random_rates()
        
        for time_scale in range(3):
            # take 10 steps
            t = range(0, 10**(time_scale + 1), 10**time_scale)
            
            _rpa_wt_ko = sci.odeint_filter(rpa_wt_ko, net, t, mask)
            if _rpa_wt_ko is None: break
            rpa_logFC = ar.logFC(*_rpa_wt_ko)
            
            # look for approximate convergence
            change = abs(np.diff(rpa_logFC, axis=1)).mean(axis=(0, 2))
            print("Change:", change[-1], "Thres:", thres)
            if change[-1] < thres:
                print("Change below threshold, approximate convergence")
                r_logFC, _, _ = Network.r_p_a(rpa_logFC)
                return net.edges(), r_logFC[:, -1, :].T, net
    


def sign_adjacency(arr, n_pk):
    """
    Provide random sign to unsigned edges for TFs.
    :param arr: array with 1s and 0s
    :param n_pk: 
    :return: inplace same array but around half of 1s are now -1s
    """
    n_tf = arr.shape[1] - n_pk
    arr[:, :n_tf] *= np.random.choice([-1, 1], size=(arr.shape[0], n_tf))
    return arr


