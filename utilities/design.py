#!/usr/bin/env python3
import colorsys
import configurations as config
from degnlib.degnutil import pandas_util as panda, string_util as st, math_util as ma, input_output as io
from colorama import init, Fore, Back, Style
init()

# constants
block_symbol = "\u2588"
ansi_reset = "\033[0m"
red_hue = 1.0
green_hue = 0.25


def hsv2rgb(h, s, v):
    """
    convert a HSV color to a RGB color.
    :param h: hue in degrees [0-1]
    :param s: saturation in percent [0-1]
    :param v: value in percent [0-1]
    :return: RGB tuple ([0-255], [0-255], [0-255])
    """
    return tuple(map(lambda x: int(round(x * 255)), colorsys.hsv_to_rgb(h, s, v)))


def rgb2ansi_fore(rgb):
    """
    :param rgb: tuple ([0-255], [0-255], [0-255])
    :return: ansi code for the color
    """
    # https://en.wikipedia.org/wiki/ANSI_escape_code#24-bit
    return "\033[38;2;" + st.join(rgb, ";") + "m"


def rgb2ansi_back(rgb):
    """
    :param rgb: tuple ([0-255], [0-255], [0-255])
    :return: ansi code for the color
    """
    # https://en.wikipedia.org/wiki/ANSI_escape_code#24-bit
    return "\033[48;2;" + st.join(rgb, ";") + "m"


def color_fore(string, h, s, v):
    """
    Color a string with 24-bit color
    :param string: string to color
    :param h: hue in degrees [0-1]
    :param s: saturation in percent [0-1]
    :param v: value in percent [0-1]
    :return: string that has color ansi code at start and reset code at end. 
    """
    return rgb2ansi_fore(hsv2rgb(h, s, v)) + string + ansi_reset


def color_back(string, h, s, v):
    """
    Color a string with 24-bit color
    :param string: string to color
    :param h: hue in degrees [0-1]
    :param s: saturation in percent [0-1]
    :param v: value in percent [0-1]
    :return: string that has color ansi code at start and reset code at end. 
    """
    return rgb2ansi_back(hsv2rgb(h, s, v)) + string + ansi_reset


def color_cell_back(value):
    """
    Color the background of a value based on how big it is on scale from -1 to 1.
    Format the resulting string to fit as a cell inside an array.
    :param value: int or float
    :return: ascii formatted color string WITHOUT end color code
    """
    progress = ma.clip((value + 1) * .5, 0, 1)
    h = ma.lerp(red_hue, green_hue, progress)
    s = ma.clip(ma.lerp(.2, 1, abs(value)), 0, 1)
    v = ma.clip(ma.lerp(.0, 2, abs(value)), 0, 1)
    ansi_color = rgb2ansi_back(hsv2rgb(h, s, v))
    s_number = "{:{width}.{precision}f}".format(value, precision=config.precision, width=config.precision+4)
    return ansi_color + s_number


def color_array(array):
    """
    Color a 2d array and return the string that can be printed
    :param array: pandas or numpy array
    :return: 
    """
    try: return color_pdarray(array)
    except AttributeError: return color_nparray(array)


def color_nparray(array):
    """
    Color a 2d numpy array and return the string that can be printed
    :param array: numpy array
    :return: 
    """
    def color_row(row):
        return " ".join([color_cell_back(cell) for cell in row]) + " " + ansi_reset
    def color_row_subset(row):
        half = int(max_cells_hori/2)-1
        cells = [color_cell_back(cell) for cell in row[:half]] \
                + [ansi_reset + " ..."] \
                + [color_cell_back(cell) for cell in row[-half:]]
        return " ".join(cells) + " " + ansi_reset
    
    # cell example including trailing space: "-12.{precision} "
    cell_width = 4 + config.precision + 1
    max_cells_vert = terminal_width = io.get_terminal_width()
    max_cells_hori = int(terminal_width / cell_width)

    color_row_func = color_row if array.shape[1] <= max_cells_hori else color_row_subset
    
    if array.shape[0] <= max_cells_vert:
        return "\n".join([color_row_func(row) for row in array])
    
    half_vert = int(max_cells_vert/2)-1
    half_hori = min(int(array.shape[1] * cell_width / 2) - 1, (int(max_cells_hori/2)-1) * cell_width + 1)
    out = [color_row_func(row) for row in array[:half_vert,]] \
          + [" " * half_hori + "..."] \
          + [color_row_func(row) for row in array[-half_vert:,]]
    return "\n".join(out)
    

def color_pdarray(array):
    """
    Color a 2d pandas array and return the string that can be printed
    :param array: pandas array
    :return: 
    """
    def _color_cell_back(value):
        try: return color_cell_back(value)
        except TypeError:
            return ansi_reset + "{:>{w}}".format(value, w=cell_width)
    
    # numpy arrays do not have this attribute
    index_width = max(len(str(ind)) for ind in array.index)
    # a cell can be e.g. "-40.<precision>" excluding trailing space
    cell_width = config.precision + 4
    max_cells_vert = terminal_width = io.get_terminal_width()
    max_cells_hori = int((terminal_width - index_width) / (cell_width + 1))
    
    # use ... if array is too big
    if array.shape[1] > max_cells_hori:
        half = int((max_cells_hori - 1) / 2)
        array = panda.remove_column_index(array, range(half, array.shape[1]-half))
        array = panda.insert_column(array, half, "..", "...")
    if array.shape[0] > max_cells_vert:
        half = int((max_cells_vert - 1) / 2)
        array = panda.remove_row_index(array, range(half, array.shape[0]-half))
        array = panda.insert_row(array, half, "..", "...")
    
    index = ["{:<{w}}".format(ind, w=index_width) for ind in array.index]
    header = ["{:>{w}}".format(h, w=cell_width) for h in array.columns]
    
    # trim too long names
    for i, ind in enumerate(index):
        # minus 1 to replace last character
        if len(ind) > index_width: index[i] = ind[:index_width-1] + "."
    for i, h in enumerate(header):
        # we add a dot or space after the cell, which is the space separating each cell
        if len(h) > cell_width: header[i] = h[:cell_width] + "."
        else: header[i] += " "
    
    header = [" " * (index_width + 1)] + header
    out = ["".join(header)]
    for i_row, (_, row) in enumerate(array.iterrows()):
        out += [" ".join([index[i_row]] + [_color_cell_back(row[col]) for col in array.columns]) + " " + ansi_reset]
    return "\n".join(out)


def dim(string):
    """
    Get a dim version of string
    :param string: 
    :return: 
    """
    return Style.DIM + string + Style.NORMAL


def bright(string):
    """
    Get a bright version of string
    :param string: 
    :return: 
    """
    return Style.BRIGHT + string + Style.NORMAL
