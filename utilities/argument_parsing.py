#!/usr/bin/env python3
import configurations as config

"""
This script is for functions related to argument parsing, 
e.g. convenience functions for arguments often used.
"""

# neural net arguments

def epoch(parser):
    parser.add_argument("-ep", "--epochs", type=int, metavar="positive integer", default=config.epochs,
                        help="Number of epochs to train.")


def n_print_training(parser):
    parser.add_argument("-prints", "--prints", type=int, metavar="positive integer",
                        help="The number of times to print train status.", default=config.n_print_training)


def lasso(parser):
    parser.add_argument("-lasso", "--lasso", type=float, default=config.lasso, metavar="positive float",
                        help="Weight of lasso. Default is 0 so turned off due to performance.")
    
def regularization(parser):
    parser.add_argument("-reg", "--regularization", choices=['l1', 'mm'], default='l1',
                        help="Regularization function, L1-norm, or Michaeles-Menten inspired.")


def gradient_descent(parser):
    choices = ['SGD', 'Adam', 'Adamax', 'Adagrad', 'Adadelta', 'ASGD', 'RMSprop', 'Rprop']
    choices += [c.lower() for c in choices]
    parser.add_argument("-descent", "--gradient-descent", choices=choices, default=config.gradient_descent,
                        help="Gradient descent method to use.")


def batch_size(parser, default=None):
    parser.add_argument("-batch", "--batch-size", type=int, default=default, 
                        help="Batch size, number of datapoints used for each gradient descent step.")


