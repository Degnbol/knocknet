#!/usr/bin/env python3
from prompt_toolkit import prompt
from prompt_toolkit.history import FileHistory
from prompt_toolkit.auto_suggest import AutoSuggestFromHistory as AutoSuggest
from prompt_toolkit.completion import Completer, Completion
from fuzzyfinder import fuzzyfinder

# constant
prompt_string = "> "

class FuzzyCompleter(Completer):
    """A word completer allowing for fuzzy typing."""

    def __init__(self, words):
        self.words = list(words)

    def get_completions(self, document, complete_event):
        word_before_cursor = document.get_word_before_cursor(WORD=True)
        matches = fuzzyfinder(word_before_cursor, self.words)
        for m in matches:
            yield Completion(m, start_position=-len(word_before_cursor))


def feature_prompt(words):
    """
    Get a prompt that is fully featured.
    :param words: list of words that should be auto-completed
    :return: the input from the user.
    """
    return prompt(prompt_string,
                  history=FileHistory("command_history.txt"),
                  auto_suggest=AutoSuggest(),
                  completer=FuzzyCompleter(words))

