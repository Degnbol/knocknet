#!/usr/bin/env python3
import numpy as np
import argparse
from degnlib.degnutil import argument_parsing as argue, read_write as rw, string_util as st


def get_parser():
    parser = argparse.ArgumentParser(description="Get a mask of the edges that are possible to observe. "
                                                 "The output is a list of 1s and 0s where each value should be used to mask PK edges, "
                                                 "i.e. 0 in first value means that PK edges going to node 0 will not be possible to observe in RNA data. "
                                                 "TF edges are of course always observable.")
    argue.infile(parser, help="Input matrix file with TF and PK edges, "
                              "from source indicated by column index and target node indicated by row index.")
    argue.outfile(parser)
    parser.add_argument("-pk", "--n-pk", required=True, type=int,
                        help="Number of protein kinases, "
                             "where it is assumed that the nodes are sorted "
                             "so the last entries along axis 1 is the PK nodes.")
    parser.add_argument("-sep", "--sep", default=" ", help="Separator character to read infile.")
    return parser


def get_args():
    args = get_parser().parse_args()
    assert args.n_pk >= 0
    return args


def main(args):
    
    edges = np.asarray(rw.read_array(args.infile, sep=args.sep))
    backward_edges = abs(edges.T)
    
    n_prot = edges.shape[1]
    n_pk = args.n_pk
    n_tf = n_prot - n_pk
    
    
    I_T = np.diag([1 for _ in range(n_tf)] + [0 for _ in range(n_pk)])

    observed = [backward_edges @ np.ones(n_prot) @ I_T]

    max_cascade = 100
    for _ in range(max_cascade):
        # go back through each edge from the observed ones, adding more nodes to the list of what is observed
        observed.append(backward_edges @ observed[-1])
        if np.all((observed[-2] == 0) == (observed[-1] == 0)): break
        
    
    # convert to boolean of dtype int
    observed = (np.sum(observed, axis=0) != 0).astype(int)
    # write
    with open(args.outfile, 'w') as fh:
        fh.write(st.join(observed, '\n') + '\n')


if __name__ == '__main__':
    main(get_args())
