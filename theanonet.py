#!/usr/bin/env python3
import argparse
from pathlib import Path
import numpy as np
import pandas as pd
import theano as th
import theano.tensor as T
from utilities import argument_parsing as argue2
from degnlib.degnutil import (array_util as ar, read_write as rw, argument_parsing as argue, input_output as io,
                              pandas_util as panda, theano_util as thea, neural_network as ne)
import configurations as config
from knocknet import Knocknet, main
th.config.floatX = "float32"

class Theanonet(Knocknet):
    
    # static and constant
    weight_keys = ['pk_on_pk', 'pk_on_tf', 'tf_on_gene']
    mask_keys = [k + "_mask" for k in weight_keys]
    data_keys = ['pk_ko', 'tf_ko', 'gene']
    constant_keys = ['gene_on_pk', 'gene_on_tf']
    pred_keys = ['gene_pred']
    matrix_keys = weight_keys + mask_keys + data_keys + constant_keys + pred_keys
    
    def __init__(self):
        super().__init__()
        self.weights = None
        self.train_func = None
        self.predict_func = None
    
    
    # argparse

    def get_commandline_functions(self):
        """
        The parsers for the commands/functions available from commandline only
        :return: 
        """
        menus = {"run": self.get_parser()}
        return {**menus, **super().get_commandline_functions()}
    
    
    def get_menu_functions(self):
        """
        The parsers only used within the program to run all menu functions and settings
        :return: None
        """
        menus = {"read-arrays": self.get_read_arrays_parser(), "init": self.get_initialize_parser(), 
                 "check": self.get_check_parser(), "define-network": self.get_define_network_parser(), 
                 "train": self.get_training_parser(), "predict": self.get_prediction_parser(),
                 "name": self.get_name_parser()}
        return {**menus, **super().get_menu_functions()}
    
    
    def get_array_parser(self):
        parser = super().get_array_parser()
        parser.add_argument("-weights", "--weights", dest="name", action="append_const", const=self.weight_keys, default=[],
                            help="add weights to the list of arrays to use.")
        parser.add_argument("-masks", "--masks", dest="name", action="append_const", const=self.mask_keys, default=[],
                            help="add masks to the list of arrays to use.")
        return parser
    
    
    def get_read_arrays_parser(self):
        
        """
        Parser that manages all the inputs to read all the necessary files for this network setup.
        :return: argparse.ArgumentParser 
        """
        parser = argparse.ArgumentParser(add_help=False, 
                                         description="Read in the necessary arrays to use in the theano network.")
        parser.set_defaults(function=self.read_arrays_function)
    
        parser.add_argument("-pk-ko", "--pk-ko", metavar="filename",
                            help="The filename for a file with a matrix of protein kinase knock-outs where each line is a datapoint.")
        parser.add_argument("-tf-ko", "--tf-ko", metavar="filename",
                            help="The filename for a file with a matrix of transcription factor knock-outs where each line is a datapoint.")
        parser.add_argument("-gene", "--gene", metavar="filename",
                            help="The filename for a file with a matrix of gene expression levels relative to wild-type. "
                                 "Each line is a datapoint. "
                                 "It is important that this file is pre-processed so it has values in range [0;1] since the "
                                 "predictions can only fall in this range and can never get accurate for values outside of it.")
        parser.add_argument("-pk-on-pk-mask", "--pk-on-pk-mask", metavar="filename",
                            help="Matrix with all possible/allowed direct connections from PK on PK. "
                                 "If a PK can regulate another PK, that connection should be 1, otherwise 0. "
                                 "As default all connections will be allowed.")
        parser.add_argument("-pk-on-tf-mask", "--pk-on-tf-mask", metavar="filename",
                            help="Matrix with all possible/allowed direct connections from PK on TF. "
                                 "If a PK can regulate a TF, that connection should be 1, otherwise 0. "
                                 "As default all connections will be allowed.")
        parser.add_argument("-tf-on-gene-mask", "--tf-on-gene-mask", metavar="filename",
                            help="Matrix with all possible/allowed direct connections from TF on gene. "
                                 "If a TF can regulate a gene, that connection should be 1, otherwise 0. "
                                 "As default all connections will be allowed.")
        parser.add_argument("-pk-on-pk", "--pk-on-pk", metavar="filename",
                            help="Previously trained pk on pk weight matrix to continue on.")
        parser.add_argument("-pk-on-tf", "--pk-on-tf", metavar="filename",
                            help="Previously trained pk on tf weight matrix to continue on.")
        parser.add_argument("-tf-on-gene", "--tf-on-gene", metavar="filename",
                            help="Previously trained tf on gene weight matrix to continue on.")
        parser.add_argument("-gene-on-pk", "--gene-on-pk", metavar="filename",
                            help="Matrix with direct connections from gene on PK. "
                                 "If a gene codes for a PK, that connection should be 1, so that if the gene has no "
                                 "activity the same is true for that PK.")
        parser.add_argument("-gene-on-tf", "--gene-on-tf", metavar="filename",
                            help="Matrix with direct connections from gene on PK. "
                                 "If a gene codes for a TF, that connection should be 1, so that if the gene has no "
                                 "activity the same is true for that TF.")
    
        return parser


    def get_initialize_parser(self):
        """
        :return: argparse.ArgumentParser 
        """
        parser = argparse.ArgumentParser(add_help=False,
                                         description="Initialize arrays for training, i.e. define arrays not read.")
        parser.set_defaults(function=self.initialize_function)

        return parser
    
    
    def get_check_parser(self):
        """
        :return: argparse.ArgumentParser 
        """
        parser = argparse.ArgumentParser(add_help=False,
                                         description="Check arrays (weights), are masks working?")
        parser.set_defaults(function=self.check_weights_function)

        return parser

    
    def get_define_network_parser(self):
        """
        :return: argparse.ArgumentParser 
        """
        parser = argparse.ArgumentParser(add_help=False,
                                         description="Define network structure.")
        parser.set_defaults(function=self.define_network_function)

        argue2.lasso(parser)
        argue2.gradient_descent(parser)

        parser.add_argument("-casc", "--max-cascade", type=int, metavar="float", default=config.max_cascade,
                            help="The number of times PK effects on PK is calculated, so the maximum length of a cascade.")
        parser.add_argument("-iter", "--iterations", type=int, metavar="integer",
                            default=config.transcription_iterations,
                            help="The number of times predicted gene expression levels will be used to recalculate protein activities.")

        return parser
    

    def get_training_parser(self):
        """
        The parser that has arguments for training only.
        :return: 
        """
        parser = argparse.ArgumentParser(add_help=False, formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                         description="Parse arguments that can describe how training should be performed.")
        parser.set_defaults(function=self.training_function)
    
        argue2.epoch(parser)
        argue2.n_print_training(parser)
        batch_args = parser.add_mutually_exclusive_group()
        argue2.batch_size(batch_args)
        batch_args.add_argument("-batch-max", "--batch-max", action="store_true",
                                help="Set the batch size to max, so all knockouts are used at the same time for gradient descent.")
        
        return parser


    def get_prediction_parser(self):
        """
        The parser that has arguments for prediction only.
        :return: 
        """
        parser = argparse.ArgumentParser(add_help=False,
                                         description="Run prediction given the network has been defined with trained weights.")
        parser.set_defaults(function=self.prediction_function)
        return parser
        
    
    def get_edge_parser(self):
        """
        Add an argument to the parent edge parser 
        so we don't have to write names of all the arrays we are interested in.
        :return: argparse.ArgumentParser
        """
        parser = super().get_edge_parser()
        parser.add_argument("-net", "--network", dest="name", action="append_const", const=self.weight_keys + self.constant_keys,
                            default=[], help="Create all edges needed for a full network.")
        return parser
    
    
    def get_node_parser(self):
        """
        Add an argument to the parent node parser 
        so we don't have to write names of all the arrays we are interested in.
        :return: argparse.ArgumentParser
        """
        parser = super().get_node_parser()
        parser.add_argument("-net", "--network", dest="name", action="append_const", const=self.data_keys, default=[],
                            help="Create all edges needed for a full network.")
        return parser
    
    
    def get_name_parser(self):
        parser = argparse.ArgumentParser(
            add_help=False, description="Function to provide naming for indexes and columns when absent for all arrays.")
        parser.set_defaults(function=self.name_function)
        return parser
    
    
    def get_parser(self):
        """
        Get all the needed arguments to run both reading, training and writing functionality in one go.
        :return: 
        """
        subparsers = [self.get_read_arrays_parser(), self.get_define_network_parser(), self.get_training_parser()]
        parser = argparse.ArgumentParser(add_help=False, parents=subparsers, conflict_handler='resolve',
                                         description="Theanonet. Read arrays, initialize, check weights, print info, define network, "
                                                     "train, predict, create node and edge tables, and write them to file.")
        parser.set_defaults(function=self.main_function)
        argue.force(parser)
        parser.add_argument("-thres", "--threshold", type=float, default=0.001,
                            help="The threshold for ignoring weak edge connections.")

        return parser
    
    
    # functions

    def main_function(self):
        print("read matrices")
        self.read_arrays_function()
        self.initialize_function()
        self.check_weights_function()
        self.info_function()
        print("define network")
        self.define_network_function()
        print("run training")
        self.training_function()
        print("run prediction")
        self.prediction_function()
        print("make edges and ndoes")
        command = "-net"
        self.node_function(argue.parse_args(self.get_node_parser(), command))
        if self.args.threshold: command += " -thres " + str(self.args.threshold)
        self.edge_function(argue.parse_args(self.get_edge_parser(), command))
        print("write edges and nodes")
        command = "edges nodes -path edges.csv nodes.csv -sep ,"
        if self.args.force: command += " --force"
        self.args = argue.parse_args(self.get_write_parser(), command)
        self.write_function()
    
    
    def read_arrays_function(self):
        """
        Read matrices if a path is given in parsed arguments.
        """
        messages = ["trained weight matrices...", "weight matrix masks...", "KO datapoints...", "constant matrices..."]
        
        for keys, message in zip([self.weight_keys, self.mask_keys, self.data_keys, self.constant_keys], messages):
            io.log.info(message)
            for key in keys:
                if key in self.arrays:
                    io.log.info("array {} already read, skipping".format(key))
                else:
                    # use what the user provided
                    try: filename = getattr(self.args, key)
                    except AttributeError: filename = None
                    # otherwise we find default
                    if not filename: filename = key + config.array_suffix
                    if Path(filename).exists():
                        print("reading", key, filename)
                        self.arrays[key] = rw.read_array(filename)
                        self.arrays = {**self.arrays, **self._get_index_columns_names(key)}
        
                            
    def _get_index_columns_names(self, key):
        """
        Create dict of the names if found in index and columns
        :param key: string key used in self.arrays
        :return: dict, either empty or e.g. {"pk_names":Index(...), "tf_names":Index(...)}
        """
        out = {}
        for idx, attr in zip(_index_columns_id(key), ("index", "columns")):
            try: names = getattr(self.arrays[key], attr)
            except AttributeError: continue
            if not isinstance(names, pd.RangeIndex): out[idx + "_names"] = names
        return out 
    
    
    def initialize_function(self):
        """
        Make sure arrays are set up correctly to use in the network.
        Unless quiet this will also print a few messages.
        """
        self.initialize_masks()
        self.initialize_weights()
    
    
    def initialize_masks(self):
        """
        Make sure masking arrays are set up correctly to use in the network.
        Unless quiet this will also print info about the net.
        """
        # make mask matrices if they are not already loaded.
        # the masks work by being the starting point of the weights, 
        # the zero values cannot be trained since there is no gradient for a value of zero.
        for key in self.mask_keys:
            if key not in self.arrays:
                self.arrays[key] = np.ones(self.get_array_shape(key), dtype='float32')
                message = "all %s connections allowed" % key[:-len("_mask")]
                if key == 'pk_on_pk_mask':
                    message += " except from a PK to itself"
                    np.fill_diagonal(self.arrays[key], 0)
                io.log.info(message)  
    
    
    def initialize_weights(self):
        """
        Initialize random values in weights if they are not already present
        """
        for key in self.weight_keys:
            if key not in self.arrays:
                io.log.info("initializing random values in weight matrix", key)
                # using the masks here as the starting point effectively limiting which weights can be nonzero
                self.arrays[key] = ar.normal_masked(self.arrays[key + '_mask'])
    
    
    def name_function(self):
        """
        Provide incrementing names for columns and rows in arrays if they are not already present.
        The names will be e.g. pk034, gene120, ko094, for a protein kinase, gene, and datapoint respectively
        :return: None
        """
        for key in self.arrays:
            if key in self.matrix_keys:
                index, columns = _index_columns_id(key)
                # get index and column names that has been read from other arrays
                try: index = self.arrays[index + "_names"]
                except KeyError: pass
                try: columns = self.arrays[columns + "_names"]
                except KeyError: pass
                
                if not panda.is_fully_named_pandas(self.arrays[key]):
                    # make an array into a pandas df with named columns
                    io.log.info("adding index and column names to " + key)
                    self.arrays[key] = panda.to_pandas(self.arrays[key], index, columns)
    
    
    def info_function(self):
        """
        Print some general info and quality checks
        """
        super().info_function()
        print("num PKs =", self.get_n_pk())
        print("num TFs =", self.get_n_tf())
        print("num genes =", self.get_n_gene())
        print("num datapoints =", self.get_n_knockouts())
        print("num parameters = %d" % self.get_n_parameters())
    
    
    def check_weights_function(self):
        """
        Verify the quality of the weight arrays.
        """
        # check weights
        for key in self.weight_keys:
            zeros_index = self.arrays[key + '_mask'] == 0
            if np.any(zeros_index):
                print("checking weight", key)
                fraction_is_zero = panda.sum(panda.index(self.arrays[key], zeros_index) == 0) / panda.sum(zeros_index)
                print("values that are zero when supposed to = {:.2f} %".format(fraction_is_zero.item() * 100))
                # correct problems
                self.arrays[key][zeros_index] = 0


    def define_network_function(self):
        inputs, self.weights, pred, target = self.network()
        self.train_func = ne.training_function(inputs, self.weights, pred, target, 
                                               self.args.gradient_descent, self.args.lasso)
        self.predict_func = ne.prediction_function(inputs, pred)
    
    
    def network(self):
        """Define the theano network of a cell, e.g. yeast. """
        mat = {key: T.fmatrix(key) for key in ['pk_ko', 'tf_ko', 'target']}

        # convert matrices to variables
        for key in self.arrays:
            if key in self.weight_keys + self.constant_keys:
                mat[key] = thea.shared(self.arrays[key], key)

        # set starting point values
        mat['pred'] = None
        mat['pk'] = T.zeros_like(mat['pk_ko'])
        mat['pk'] = knockout(mat['pk'], mat['pk_ko'])

        # go through the network
        for gene_iteration in range(self.args.iterations):
            for cascade in range(self.args.max_cascade):
                mat['pk'] = thea.dotT(mat['pk_on_pk'], mat['pk'])
                if mat['pred'] is not None: mat['pk'] = mat['pk'] + thea.dotT(mat['gene_on_pk'], mat['pred'])
                mat['pk'] = T.tanh(mat['pk'])
                mat['pk'] = knockout(mat['pk'], mat['pk_ko'])

            mat['tf'] = thea.dotT(mat['pk_on_tf'], mat['pk'])
            if mat['pred'] is not None: mat['tf'] = mat['tf'] + thea.dotT(mat['gene_on_tf'], mat['pred'])
            mat['tf'] = T.tanh(mat['tf'])
            mat['tf'] = knockout(mat['tf'], mat['tf_ko'])
            mat['gene'] = thea.dotT(mat['tf_on_gene'], mat['tf'])
            mat['pred'] = T.tanh(mat['gene'])

        # define the groups of variables needed for training and prediction
        inputs = [mat['pk_ko'], mat['tf_ko']]
        weights = [mat['pk_on_pk'], mat['pk_on_tf'], mat['tf_on_gene']]
        pred = mat['pred']
        target = mat['target']
        return inputs, weights, pred, target
    

    def training_function(self):
        # handle args
        if self.args.batch_max: self.args.batch_size = self.get_n_knockouts()
        # run training
        outputs = ne.train(self.train_func, [self.arrays['pk_ko'], self.arrays['tf_ko']], self.arrays['gene'], 
                           config.epoch, self.args.epochs, self.args.prints, self.args.batch_size)
        # update current epoch for continued training
        config.epoch += self.args.epochs
        
        
        io.log.info("update the list of arrays with the trained weight values")
        for weight in self.weights:
            self.arrays[str(weight)] = thea.variable_to_array(weight)
        
        if "training" in self.arrays:
            self.arrays["training"] = pd.concat((self.arrays["training"], outputs))
        else:
            self.arrays["training"] = outputs


    def prediction_function(self):
        """
        Perform prediction given a defined and trained network
        :return: 
        """
        pred = self.predict_func(self.arrays['pk_ko'], self.arrays['tf_ko'])
        io.log.info("update the arrays with the prediction")
        self.arrays[self.pred_keys[0]] = pred
        self.name_function()
    
    
    # getters
    
    
    def get_n_pk(self):
        """
        Get the number of protein kinases
        :return: int
        """
        return self.arrays['pk_ko'].shape[1]
    
    
    def get_n_tf(self):
        """
        Get the number of transcription factors
        :return: int
        """
        return self.arrays['tf_ko'].shape[1]
    
    
    def get_n_gene(self):
        """
        Get the number of genes that are measured
        :return: int 
        """
        return self.arrays['gene'].shape[1]
    
    
    def get_n_knockouts(self):
        """
        Get the number of datapoints (knockouts)
        :return: int 
        """
        return self.arrays['gene'].shape[0]
    
    
    def get_n_parameters(self):
        """
        Count number of trainable parameters by summing over all nonzero values in all masks.
        :return: int
        """
        return sum(panda.sum(self.arrays[key]) for key in self.mask_keys)
    
    
    def get_array_shape(self, key):
        """
        Get the shape of a matrix either using the array itself or the arrays it is connected to in the network.
        :param key: key in dict arrays
        :return: tuple shape
        """
        try: return np.shape(self.arrays[key])
        except KeyError:
            key = key.split("_")
            from_key = key[key.index("on") - 1]
            to_key = key[key.index("on") + 1]
            # if we don't have the array, there might be a knockout version which will have the same shape
            if from_key not in self.arrays: from_key += '_ko'
            if to_key not in self.arrays: to_key += '_ko'
            return self.arrays[to_key].shape[1], self.arrays[from_key].shape[1]
    


def knockout(tensor, indices):
    """
    Enforce knockout by explicitly setting knockout positions to -1.
    :param tensor: a theano tensor to change.
    :param indices: floating point indexing tensor to choose the positions to change where 0 means it should NOT be changed.
    :return: The changed tensor, where the positions are set to -1.
    """
    assert tensor is not None and indices is not None
    if not T.any(indices): return tensor
    return thea.set_indices(tensor, T.nonzero(indices), -1)


def _index_columns_id(name):
    """
    Get a string, e.g. pk, tf, gene, ko, 
    describing what the index and column names in the array with a given key describes.
    :param name: string name describing the array, e.g. "gene_on_pk", or "tf_ko"
    :return: (string index id, string columns id)
    
    examples:
    "gene_on_pk" -> ("pk", "gene")
    """
    try: columns, _, index = name.split("_")[0:3]
    except ValueError: columns, index = name.split("_")[0], "ko"
    return index, columns


if __name__ == '__main__':
    # instantiate a new Theanonet 
    main(Theanonet())

