#!/usr/bin/env python3
import sys
import argparse
import pandas as pd
from degnlib.degnutil import read_write as rw, pandas_util as panda, array_util as ar
import matplotlib
if sys.platform == "darwin": matplotlib.use("TkAgg")
from matplotlib import pyplot as plt


def get_parser(add_help=True):
    parser = argparse.ArgumentParser(add_help=add_help,
        description="Plot column(s) from table(s) as line plots with a shared x-axis.")
    parser.add_argument("tables", nargs='+', help="Tables with columns to plot.")
    parser.add_argument("-x", "--x", default=0, 
                        help="Column name or index of column to plot as x-axis.")
    parser.add_argument("-y", "--y", nargs='*', action="append", default=[], 
                        help="Column name(s) or index(es) of column(s) to plot as y-axis. "
                             "Provide this argument a second time to get separate y-axis, e.g. -x 'x' -y 'y1' -y 'y2'.")
    parser.add_argument("-title", "--title", help="Title for the plot.")
    parser.add_argument("-out", "--outfile", help="Path to write plot to.")
    parser.add_argument("-m", "--marker", default="-", help="Marker style, e.g. '-' or '.'.")
    
    return parser


def get_args():
    parsed_args = get_parser().parse_args()
    if len(parsed_args.y) > 2:
        raise argparse.ArgumentTypeError("Cannot have more than two separate y-axis.")
    return parsed_args


def main(args):
    # transform 1D arrays to column arrays
    for i, table in enumerate(args.tables):
        if table.ndim == 1:
            args.tables[i] = ar.ascol(table)
    # collect to one table
    table = pd.DataFrame(panda.concat(args.tables))
    # handle args based on read table
    if len(args.y) == 0: args.y = [c for c in table.columns if c != args.x]
    
    colors = ['tab:red', 'tab:blue', 'tab:green', 'tab:magenta']
    
    fig, ax1 = plt.subplots()
    if args.title: ax1.set_title(args.title)
    
    x = panda.col(table, args.x)
    
    ax1.set_xlabel(args.x)
    ax1.set_ylabel(", ".join(args.y[0]), color=colors[0])
    for i, y in enumerate(args.y[0]):
        ax1.plot(x, panda.col(table, y), args.marker, color=colors[i])
    ax1.tick_params(axis='y', labelcolor=colors[0])

    if len(args.y) > 1:
        # instantiate a second axes that shares the same x-axis
        ax2 = ax1.twinx()
    
        # we already handled the x-label with ax1.
        ax2.set_ylabel(", ".join(args.y[1]), color=colors[len(args.y[0])])
        for i, y in enumerate(args.y[1]):
            ax2.plot(x, panda.col(table, y), args.marker, color=colors[len(args.y[0]) + i])
        ax2.tick_params(axis='y', labelcolor=colors[len(args.y[0])])
    
    fig.tight_layout()
    if args.outfile:
        plt.savefig(args.outfile)
        plt.clf()
    else: plt.show()
    


if __name__ == '__main__':
    parsed_args = get_args()
    # read before main so other scripts can use main with memory arrays instead of file paths
    parsed_args.tables = [rw.read_array(p) for p in parsed_args.tables]
    main(parsed_args)
