#!/usr/bin/env python3
import numpy as np
import argparse
from pathlib import Path
from degnlib.degnutil import read_write as rw, argument_parsing as argue


"""
Preprocess DREAM4 challenge data. 
"""

def get_logFC_parser():
    parser = argparse.ArgumentParser(add_help=False,
        description="Create log fold change gene array from knockout and wildtype file.")
    parser.set_defaults(function=logFC_function)
    parser.add_argument("ko", help="knockout measurements")
    parser.add_argument("wt", help="Wildtype measurements")
    parser.add_argument("-out", "--outfile", default="gene.mat", 
                        help="Name of resulting log fold change gene file.")
    return parser


def get_knockout_parser():
    parser = argparse.ArgumentParser(add_help=False,
                                     description="Create a knockout boolean indexing array using a table with KO integer indexes.")
    parser.set_defaults(function=knockout_function)
    parser.add_argument("ko", help="knockout indexes")
    parser.add_argument("-n", "--nodes", required=True, type=int, help="Number of nodes")
    parser.add_argument("-single", action="store_true", 
                        help="Add single KO index as diag of 1s before multiple KO indexes.")
    parser.add_argument("-out", "--outfile", default="ko.mat",
                        help="Name of resulting ko bool indexing matrix.")
    return parser


def get_weight_parser():
    parser = argparse.ArgumentParser(add_help=False,
                                     description="Create a weight matrix using edge file.")
    parser.set_defaults(function=weight_function)
    parser.add_argument("edges", help="Edge file")
    parser.add_argument("-n", "--nodes", required=True, type=int, help="Number of nodes")
    parser.add_argument("-out", "--outfile", default="weight.mat",
                        help="Name of resulting matrix.")
    return parser


def get_parser():
    parser = argparse.ArgumentParser("Preprocess DREAM4 data")
    subparsers = {"logFC": get_logFC_parser(),
                  "ko": get_knockout_parser(),
                  "weight": get_weight_parser(),}
    argue.add_subparsers(parser, subparsers)
    return parser


def get_args():
    return get_parser().parse_args()


def logFC(observed, wildtype, KO_fraction=0.05):
    """
    Return a log2 fold change version given observed and wildtype measurements of genes.
    :param observed: array with 1 experiment in each row.
    :param wildtype: array with 1 row.
    :param KO_fraction: if a KO is perfect we get NaN from log so those values are replaced.
    The KO_fraction is the fraction of wildtype that a KO is then instead considered to have. 
    :return: array with 1 experiment in each row.
    """
    observed, wildtype = np.asarray(observed), np.asarray(wildtype)
    # give wildtype the same shape as observed matrix
    wildtype = np.repeat(wildtype, observed.shape[0], axis=0)
        
    relative = observed / wildtype
    relative[relative == 0] = KO_fraction
    return np.log2(relative)
    

def logFC_function(args):
    # we assume there is 1 experiment per row
    gene = logFC(rw.read_array(args.ko), rw.read_array(args.wt))
    # we transpose to have 1 experiment per column
    rw.write_array(gene.T, args.outfile)


def int2logical(indexes, n):
    """
    Convert int indexes to boolean array with those indexes set to true
    Each column is indexed,
    so if indexes[:,0] == [1, 3] => out[:,0] == [0,1,0,1] for n=4
    :param indexes: 
    :param n: 
    :return: 
    """
    shape1 = indexes.shape[1]
    out = np.zeros((n, shape1), dtype=bool)
    for i in range(shape1):
        # insert 1s in a column at the int indexes
        out[indexes[:, i], i] = True
    return out


def knockout_function(args):
    # transpose since we assume infile has 1 experiment per row and we want 1 per column
    ko_indexes = np.asarray(rw.read_array(args.ko)).T
    # we assume indexes are 1-indexed
    ko_indexes -= 1
    ko = int2logical(ko_indexes, args.nodes).astype(int)
    
    if args.single:
        single = np.eye(args.nodes, dtype=int)
        ko = np.concatenate((single, ko), axis=1)
    
    rw.write_array(ko, args.outfile)    


def weight_function(args):
    weight = np.zeros((args.nodes, args.nodes), dtype=int)
    for line in Path(args.edges).open('r'):
        fro, to, val = line.strip().split()
        # from and to are called e.g. "G4" and 1-indexed
        fro, to, val = int(fro[1:]) - 1, int(to[1:]) - 1, int(val)
        weight[to, fro] = val
    
    rw.write_array(weight, args.outfile)

if __name__ == '__main__':
    args = get_args()
    args.function(args)


