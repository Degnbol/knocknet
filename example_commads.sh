#!/usr/bin/env bash
# find folder with data
cd ../data_small
# start program
knocknet
# see functions
help
# see settings
get
# set lasso value. 
# Note setting values with set often only changes a default value used in other functions
set lasso 0.01
# see what arrays will be used in the network if read
read-arrays -h
# read arrays that can be used in the network, assuming they have default naming and are in current working dir
read-arrays
# see list of arrays read
array
# get verbose info about a given array
array gene -v
# initialize arrays that are not read, which is e.g. allowing all possible edges (no sparsity) if no mask arrays were read
init
# make sure all edges that are not allowed to exist actually doesn't exist
check
# get some basic info
info
# see parameters for setting up the network, the lasso value set before is default value for this function. 
# That means this function has to be run to change the lasso value.
define-network -h
# set up the network, define gradients and cost function
define-network
# run training with some settings that are not default
train -ep 500 -batch 3
# you can write weights to file if you want to keep them for another training
write -weights
# run prediction using the trained network to get a prediction of the gene expression profile
predict
# compare your predicted expression profile to the real one
array gene gene_pred -v
# save it as an image
imshow gene -title gene
imshow gene_pred -title "gene prediction"
# open the .pdf files
bash open gene.pdf gene_pred.pdf
# make edges and nodes of the network to be read into cytoscape
edge -net -thres 0.01
node -net
# inspect the edge table
array edges
# write them to file
write edges nodes -path edges.csv nodes.csv -sep ,
# import edges as network into cytoscape, then import nodes as table. 
# Weight parameter describes activation/repression.
# quit program
quit

# run the whole thing in one command, overwriting existing files
knocknet run -f
