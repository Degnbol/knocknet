#!/usr/bin/env python3
import argparse
import numpy as np
import pickle
from knocknet import main
from ebertorch import Ebertorch
import gnw
from degnlib.degnutil import input_output as io, read_write as rw
np.set_printoptions(suppress=False)


class Gnwnet(Ebertorch):
    
    def __init__(self):
        super().__init__()
        self.net = None
    
    
    def get_menu_functions(self):
        menus = {"write-net": self.get_write_net_parser(),}
        return {**menus, **super().get_menu_functions()}
    
    
    # argparse
    
    def get_synthesize_parser(self):
        parser = argparse.ArgumentParser(
            add_help=False, 
            description="Synthesize gene reads using custom GNW. "
                        'Saves edges to "edges" and reads to "gene". '
                        'Uses ko if among the arrays.')
        parser.set_defaults(function=self.synthesize_function)
        parser.add_argument("adjacency", help="Unsigned edges for nodes")
        parser.add_argument("n_pk", nargs="?", type=int, help="Number of PK")
        parser.add_argument("n_tf", nargs="?", type=int, help="Number of TF")
        parser.add_argument("-rand-sign", "--random-sign", action="store_true", 
                            help="Set flag to randomize sign of TFs.")
        
        return parser
    
    
    def get_write_net_parser(self):
        parser = argparse.ArgumentParser(
            add_help=False,
            description="Write the GNW class instance to file.")
        parser.set_defaults(function=self.write_net_function)
        parser.add_argument("path", help="Path to write to.")
        return parser
    
    
    # functions
    
    
    def synthesize_function(self):
        self.parse_n_tf_pk()
        adjacency = self.arrays[self.args.adjacency].astype(float)
        if self.args.random_sign:
            adjacency = gnw.sign_adjacency(adjacency, self.n_pk)
        # make PK edges from 0.5 to 1.0
        # adjacency[:, -self.args.n_pk:] *= np.random.uniform(0.5, 1, (adjacency.shape[0], self.args.n_pk))
        knockout = self.arrays["ko"] if "ko" in self.arrays else None
        self.arrays["edges"], self.arrays["gene"], self.net = gnw.synthesize(adjacency, self.n_pk, knockout)
        io.log.debug("we fill the diag of edges with zeros since self edges are not considered.")
        np.fill_diagonal(self.arrays["edges"], 0)
        io.log.warning("synthesis complete, edges have been changed")
        
    
    def write_net_function(self):
        rw.write_class(self.net, self.args.path)



if __name__ == '__main__':
    # instantiate a new Ebernet 
    main(Gnwnet())

