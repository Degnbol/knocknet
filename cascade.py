#!/usr/bin/env python3
import numpy as np
import argparse
import torch as tc
from degnlib.degnutil import argument_parsing as argue, read_write as rw, string_util as st


def get_parser():
    parser = argparse.ArgumentParser(description="")
    argue.infile(parser, help="Input matrix file with edges, "
                              "from source indicated by column index and target node indicated by row index.")
    argue.outfile(parser)
    parser.add_argument("-pk", "--n-pk", required=True, type=int,
                        help="Number of protein kinases, "
                             "where it is assumed that the nodes are sorted "
                             "so the last entries along axis 1 is the PK nodes.")
    parser.add_argument("-sep", "--sep", default=" ", help="Separator character to read infile.")
    return parser


def get_args():
    args = get_parser().parse_args()
    assert args.n_pk >= 0
    return args


def cascade(W, n_pk):
    # make square and abs
    W = abs(W[:W.shape[1], :])

    n_prot = W.shape[1]
    n_tf = n_prot - n_pk

    I_T = np.diag([1 for _ in range(n_tf)] + [0 for _ in range(n_pk)])
    I_P = np.diag([0 for _ in range(n_tf)] + [1 for _ in range(n_pk)])

    observed = [I_T @ np.ones(n_prot)]

    max_cascade = 20
    for _ in range(max_cascade):
        # go back through each edge from the observed ones, adding more nodes to the list of what is observed
        observed.append((W @ I_P).T @ observed[-1])
        if np.all((observed[-2] == 0) == (observed[-1] == 0)): break

    return np.sum(observed[1:], axis=0)


def cascade_torch(W, I_T, I_P):
    n_prot = W.shape[1]
    # make square and abs
    W = abs(W[:n_prot, :])
    observed = [I_T[:n_prot, :n_prot] @ tc.ones(n_prot)]

    max_cascade = 100
    for _ in range(max_cascade):
        # go back through each edge from the observed ones, adding more nodes to the list of what is observed
        observed.append((W @ I_P[:n_prot, :n_prot]).t() @ observed[-1])
        if all((observed[-2] == 0) == (observed[-1] == 0)): break

    return tc.stack(observed[1:], dim=0).sum(dim=0)



def main(args):
    W = np.asarray(rw.read_array(args.infile, sep=args.sep))
    observed = cascade(W, args.n_pk)
    with open(args.outfile, 'w') as fh:
        fh.write(st.join(observed, '\n') + '\n')


if __name__ == '__main__':
    main(get_args())
