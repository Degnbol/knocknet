#!/usr/bin/env python3
import argparse
from pathlib import Path
import numpy as np
import pandas as pd
import torch as tc
from sklearn.metrics import average_precision_score
from utilities import argument_parsing as argue2
from degnlib.degnutil import (array_util as ar, read_write as rw, argument_parsing as argue, input_output as io,
                              pandas_util as panda, torch_util as torch, string_util as st)
import configurations as config
from knocknet import Knocknet, main
from cascade import cascade_torch as cascade
import logging
np.set_printoptions(suppress=False)


class Ebertorch(Knocknet):
    
    # static and constant
    weight_keys = ['weight']
    mask_keys = ['mask', 'sign_mask', 'reg_mask']
    data_keys = ['ko', 'gene', 'tf_ko', 'pk_ko']
    
    
    def __init__(self):
        super().__init__()
        # W is the shared variable version of self.arrays['weight']
        self.W, self.n_pk, self.n_tf, self.model, self.model_pred, self.model_B = [None for _ in range(6)]
        self.Wdiag, self.Bdiag = True, False
    
    
    # argparse

    def get_commandline_functions(self):
        """
        The parsers for the commands/functions available from commandline only
        :return: 
        """
        menus = {"run": self.get_parser()}
        return {**menus, **super().get_commandline_functions()}
    
    
    def get_menu_functions(self):
        """
        The parsers only used within the program to run all menu functions and settings
        :return: dict of menu keys and their parsers
        """
        menus = {"read-arrays": self.get_read_arrays_parser(), "init": self.get_initialize_parser(), 
                 "check": self.get_check_parser(), "define-network": self.get_define_network_parser(),
                 "train": self.get_training_parser(), "predict": self.get_prediction_parser(), 
                 "synth": self.get_synthesize_parser(), "B2W": self.get_B2W_parser(), "tfpk": self.get_tfpk_parser(),
                 "set": self.get_set_parser(),}
        return {**menus, **super().get_menu_functions()}
    
    
    def get_array_parser(self):
        parser = super().get_array_parser()
        parser.add_argument("-weights", "--weights", dest="name", action="append_const", const=self.weight_keys, default=[],
                            help="add weights to the list of arrays to use.")
        parser.add_argument("-masks", "--masks", dest="name", action="append_const", const=self.mask_keys, default=[],
                            help="add masks to the list of arrays to use.")
        return parser
    
    
    def get_read_arrays_parser(self):  
        """
        Parser that manages all the inputs to read all the necessary files for this network setup.
        :return: argparse.ArgumentParser 
        """
        parser = argparse.ArgumentParser(add_help=False, 
                                         description="Read in the necessary arrays to use in the theano network.")
        parser.set_defaults(function=self.read_arrays_function)
    
        parser.add_argument("-ko", "--ko", metavar="filename",
                            help="The filename for a file with a matrix of knockouts where each row is a datapoint. "
                                 "This should include both TFs and PKs, with TF rows before PK rows.")
        parser.add_argument("-pk-ko", "--pk-ko", metavar="filename",
                            help="The filename for a file with a matrix of PK knockouts where each row is a datapoint.")
        parser.add_argument("-tf-ko", "--tf-ko", metavar="filename",
                            help="The filename for a file with a matrix of TF knockouts where each row is a datapoint.")
        parser.add_argument("-gene", "--gene", metavar="filename",
                            help="The filename for a file with a matrix of gene expression levels relative to wild-type. "
                                 "Each row is a datapoint. ")
        
        return parser
    
    
    def get_initialize_parser(self):
        """
        :return: argparse.ArgumentParser 
        """
        parser = argparse.ArgumentParser(add_help=False,
                                         description="Initialize arrays for training, i.e. define arrays not read.")
        parser.set_defaults(function=self.initialize_function)
        parser.add_argument("pval", nargs="?", help="Key for array with pvals to use for scaling the std of weight scalars.")

        return parser
    
    
    def get_check_parser(self):
        """
        :return: argparse.ArgumentParser 
        """
        parser = argparse.ArgumentParser(add_help=False,
                                         description="Check arrays (weights), are masks working?")
        parser.set_defaults(function=self.check_weights_function)

        return parser


    def get_set_parser(self):
        parser = argparse.ArgumentParser(add_help=False, formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                         description="Set or get a field of the class.")
        parser.set_defaults(function=self.set_function)
        parser.add_argument("variable", help="Class field to get or set.")
        parser.add_argument("value", nargs="?", help="Value to assign it.")
        return parser


    def get_define_network_parser(self):
        parser = argparse.ArgumentParser(add_help=False, formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                         description="Parse arguments that define the network so it is ready for training and prediction.")
        parser.set_defaults(function=self.define_network_function)
        parser.add_argument("-tf-reg", "--tf-reg", type=float, default=0.01, metavar="positive float",
                            help="Regularization strength on TF edges.")
        parser.add_argument("-pk-reg", "--pk-reg", type=float, default=0.001, metavar="positive float",
                            help="Regularization strength on TF edges.")
        parser.add_argument("-known-reg", "--known-reg", type=float, default=0.4, metavar="positive float",
                            help="Regularization strength on known edges.")
        parser.add_argument("-cas-reg", "--cascade-reg", type=float, default=0.0, metavar="positive float",
                            help="Cascade regularization strength. Should NEVER be larger than pk-reg.")
        argue2.regularization(parser)
        parser.add_argument("-n-pk", "--n-pk", type=int, help="Number of protein kinases. n_pk + n_tf = n_gene.")
        parser.add_argument("-n-tf", "--n-tf", type=int, help="Number of transcription factors. n_pk + n_tf = n_gene.")
        return parser

    
    def get_training_parser(self):
        parser = argparse.ArgumentParser(add_help=False, formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                         description="Parse arguments that can describe how training should be performed.")
        parser.set_defaults(function=self.training_function)
        argue2.gradient_descent(parser)
        argue2.batch_size(parser)
        argue2.epoch(parser)
        argue2.n_print_training(parser)
        
        return parser


    def get_prediction_parser(self):
        """
        The parser that has arguments for prediction only.
        :return: 
        """
        parser = argparse.ArgumentParser(add_help=False,
                                         description="Run prediction given the network has been defined with trained weights.")
        parser.set_defaults(function=self.prediction_function)
        return parser
        
    
    def get_edge_parser(self):
        """
        Add an argument to the parent edge parser 
        so we don't have to write names of all the arrays we are interested in.
        :return: argparse.ArgumentParser
        """
        parser = super().get_edge_parser()
        parser.add_argument("-weight", "--weight", dest="name", action="append_const", const=["weight"],
                            default=[], help="Create all edges needed for a full network.")
        return parser
    
    
    def get_node_parser(self):
        """
        Add an argument to the parent node parser 
        so we don't have to write names of all the arrays we are interested in.
        :return: argparse.ArgumentParser
        """
        parser = super().get_node_parser()
        parser.add_argument("-net", "--network", dest="name", action="append_const", const=self.data_keys, default=[],
                            help="Create all edges needed for a full network.")
        return parser
    
    
    def get_synthesize_parser(self):
        parser = argparse.ArgumentParser(
            add_help=False, 
            description="Given edges for the net stored in self.arrays['weight'], "
                        "calculate node values iteratively until convergence or until stopped.")
        parser.set_defaults(function=self.synthesize_function)
        parser.add_argument("-tol", "--tolerance", type=float, default=1e-12,
                            help="How much different a value is before we say that they are the same and convergence is reached.")
        parser.add_argument("-its", "--iterations", type=int, default=10000, help="Upper limit for number of iterations to perform.")

        return parser


    def get_B2W_parser(self):
        parser = argparse.ArgumentParser(add_help=False, formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                         description="Given a matrix B get the corresponding matrix W.")
        parser.set_defaults(function=self.B2W_function)
        argue2.gradient_descent(parser)
        argue2.epoch(parser)
        argue2.n_print_training(parser)

        return parser


    def get_tfpk_parser(self):
        parser = argparse.ArgumentParser(add_help=False, formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                         description="Create a tf_* and pk_* subsection of an array, "
                                                     "that is split so the first n_tf columns goes to tf_* array.")
        parser.set_defaults(function=self.tfpk_function)
        parser.add_argument("name", nargs="+", help="The name of the array(s) to split.")
        parser.add_argument("-n-tf", type=int)
        parser.add_argument("-n-pk", type=int)
        
        return parser

    
    def get_parser(self):
        """
        Get all the needed arguments to run both reading, training and writing functionality in one go.
        :return: 
        """
        subparsers = [self.get_read_arrays_parser(), self.get_define_network_parser(), self.get_training_parser()]
        parser = argparse.ArgumentParser(add_help=False, parents=subparsers, conflict_handler='resolve',
                                         description="Ebernet. Read arrays, initialize, check weights, print info, define network, "
                                                     "train, predict, create node and edge tables, and write them to file.")
        parser.set_defaults(function=self.main_function)
        argue.force(parser)
        parser.add_argument("-thres", "--threshold", type=float, default=0.001,
                            help="The threshold for ignoring weak edge connections.")

        return parser
    
    
    # functions

    def main_function(self):
        print("read matrices")
        self.read_arrays_function()
        self.initialize_function()
        print("define network")
        self.define_network_function()
        self.check_weights_function()
        self.info_function()
        print("run training")
        self.training_function()
        print("run prediction")
        self.prediction_function()
        print("make edges and nodes")
        command = "-net"
        self.node_function(argue.parse_args(self.get_node_parser(), command))
        if self.args.threshold: command += " -thres " + str(self.args.threshold)
        self.edge_function(argue.parse_args(self.get_edge_parser(), command))
        print("write edges and nodes")
        command = "edges nodes -path edges.csv nodes.csv -sep ,"
        if self.args.force: command += " --force"
        self.args = argue.parse_args(self.get_write_parser(), command)
        self.write_function()
    
    
    def set_function(self):
        if self.args.value:
            value = st.parse(self.args.value)
            setattr(self, self.args.variable, value)
        else:
            print(getattr(self, self.args.variable))
    
    
    def read_arrays_function(self):
        """
        Read matrices if a path is given in parsed arguments.
        """
        def get_filename(key):
            # use what the user provided
            try: filename = getattr(self.args, key)
            except AttributeError: filename = None
            if filename is not None: return filename
            # otherwise we find default
            return key + config.array_suffix
        
        messages = ["trained weights matrices...", "weights matrix masks...", "KO datapoints..."]
        
        for keys, message in zip([self.weight_keys, self.mask_keys, self.data_keys], messages):
            io.log.info(message)
            for key in keys:
                if key in self.arrays:
                    io.log.info("array {} already read, skipping".format(key))
                else:
                    filename = get_filename(key)
                    if Path(filename).exists():
                        print("reading", key, filename)
                        self.arrays[key] = rw.read_array(filename)
        
        # use tf_ko and pk_ko if given instead of ko
        if "tf_ko" in self.arrays and "pk_ko" in self.arrays and "ko" not in self.arrays:
            self.arrays["ko"] = panda.concat([self.arrays["tf_ko"], self.arrays["pk_ko"]])
        
    
    def initialize_function(self):
        """
        Make sure arrays are set up correctly to use in the network.
        Unless quiet this will also print a few messages.
        """
        print("setting starting epoch to 0")
        config.epoch = 0
        self.initialize_weight()
        self.arrays['weight'] = self.mask(self.arrays['weight'])
        # not so necessary but let's add it for good measure
        self.arrays['mask'] = self.get_mask()
    
    
    def mask(self, weight):
        """
        Mask the weight.
        """
        # make mask matrix if not already loaded.
        # the masks work by being the starting point of the weights, 
        # the zero values cannot be trained since there is no gradient for a value of zero.
        if self.Wdiag:
            weight = mask(weight)
            io.log.info("self connections removed (diagonal of weight matrix)")

        # remove PK to gene edges if W is not square
        if weight.shape[0] > weight.shape[1]:
            if "mask" not in self.arrays: self.arrays["mask"] = np.ones(weight.shape)
            try:
                self.arrays["mask"].iloc[self.n_tf + self.n_pk:, self.n_tf:] = 0
            except AttributeError:
                self.arrays["mask"][self.n_tf + self.n_pk:, self.n_tf:] = 0
                
        
        if "mask" in self.arrays:
            weight = mask(weight, self.arrays["mask"])
            io.log.info("mask applied")
        if "TF_mask" in self.arrays:
            weight = self.mask_TF(weight, self.arrays["TF_mask"])
            io.log.info("TF mask applied")
        if "PK_mask" in self.arrays:
            weight = self.mask_PK(weight, self.arrays["PK_mask"])
            io.log.info("PK mask applied")
        return weight
    
    
    def initialize_weight(self):
        """
        Initialize random values in weights if they are not already present
        """
        if 'weight' not in self.arrays:
            io.log.info("initializing random values in weight matrix")
            # using the masks here as the starting point effectively limiting which weights can be nonzero
            n = self.get_n_gene()
            try: m = self.n_tf + self.n_pk
            except TypeError: m = None
            self.arrays['weight'] = ar.noise(n, m)
            
            if self.args.pval:
                pval = self.arrays[self.args.pval]
                io.log.info("std for noise is scaled with 1.0001 - pval")
                self.arrays['weight'][:pval.shape[0], :pval.shape[1]] *= 1.0001 - pval
    
    
    def info_function(self):
        """
        Print some general info and quality checks
        """
        super().info_function()
        print("num PKs =", self.n_pk)
        print("num TFs =", self.n_tf)
        print("num genes =", self.get_n_gene())
        print("num datapoints =", self.get_n_knockouts())
        print("num parameters = %d" % self.get_n_parameters())
        if "gene" in self.arrays: print("gene shape:", self.arrays["gene"].shape)
        if "ko" in self.arrays: print("ko shape:", self.arrays["ko"].shape)
        if "weight" in self.arrays: print("weight shape:", self.arrays["weight"].shape)
    
    
    def check_weights_function(self):
        """
        Verify the quality of the weights arrays.
        """
        # check weights
        zeros_index = self.get_mask() == 0
        fraction_is_zero = panda.sum(panda.index(self.arrays['weight'], zeros_index) == 0) / panda.sum(zeros_index)
        level = logging.ERROR if fraction_is_zero < 1 else logging.INFO
        io.log.log(level, "values that are zero when supposed to = {:.2f} %".format(fraction_is_zero.item() * 100))


    def define_network_function(self):
        self.parse_n_tf_pk()
        
        # redo mask, so there is no mistakes
        self.arrays['weight'] = self.mask(self.arrays['weight'])
        
        self.set_W()
        mask = self.arrays.get("mask", None)
        sign_mask = self.arrays.get("sign_mask", None)
        reg_mask = self.arrays.get("reg_mask", None)
        reg = {"l1": ar.l1, "mm": ar.mm_norm}[self.args.regularization]
        reg_lambda = {"TF": self.args.tf_reg, "PK": self.args.pk_reg, "known": self.args.known_reg, "cascade": self.args.cascade_reg}
        
        self.model = self.get_model(self.W, reg_lambda, mask, sign_mask, reg_mask, reg)
        self.model_pred = self.get_model_pred(self.W)
        if "B" in self.arrays:
            self.model_B = self.get_model_B(self.arrays['B'], self.W, reg_lambda, mask, sign_mask, reg_mask, reg)
    

    def training_function(self):
        # manage args
        assert self.arrays['gene'].shape == self.arrays['ko'].shape
        assert self.arrays['weight'].shape[0] == self.arrays['gene'].shape[0], \
            "weight: {}, gene: {}".format(self.arrays['weight'].shape, self.arrays['gene'].shape)
        if not self.args.batch_size: self.args.batch_size = self.get_n_knockouts()

        inputs = self.arrays['ko'], self.arrays['gene']
        optimizer = torch.parse_optim(self.args.gradient_descent)([self.W])
        
        outputs = torch.train_batch(
            self.model, inputs, optimizer, self.args.epochs, self.args.batch_size, self.args.prints, config.epoch, self._abs_aps())
        self.arrays["weight"] = torch.toarray(self.W)
        # if using sign_mask the weights are forced to have a certain sign with relu. 
        # The weight is reversed in case of known negative sign so we reverse again to get the weight actually used
        if "sign_mask" in self.arrays:
            self.arrays["weight"][self.arrays["sign_mask"] != 0] = abs(self.arrays["weight"][self.arrays["sign_mask"] != 0])
            self.arrays["weight"][self.arrays["sign_mask"] == -1] *= -1
        # I don't want the remove diag arg here because it will be another input arg for the function. 
        # If you don't want diag, just imagine it is not there when viewing the B
        _B = self.B(self.W, self.I_TF(), self.I_PK())
        self.arrays["B"] = torch.toarray(_B)
        
        # update current epoch for continued training
        config.epoch += self.args.epochs        
        
        if "training" in self.arrays:
            self.arrays["training"] = pd.concat((self.arrays["training"], outputs))
        else:
            self.arrays["training"] = outputs


    def prediction_function(self):
        """
        Perform prediction given a trained network
        """
        assert self.arrays['gene'].shape == self.arrays['ko'].shape
        assert self.arrays['weight'].shape[0] == self.arrays['gene'].shape[0], \
            "weight: {}, gene: {}".format(self.arrays['weight'].shape, self.arrays['gene'].shape)

        pred = self.model_pred(self.arrays['ko'], self.arrays['gene'])
        self.arrays["gene_pred"] = torch.toarray(pred)
    
    
    def synthesize_function(self):
        """
        Given knockouts and edges for the net, calculate node values iteratively until convergence or until stopped.
        This function is similar to predict, except it keeps going and assumes the given weights are true.
        :return: 
        """

        # synthesize knockouts as gaussian with mean=-4, std=1
        self.arrays["gene"] = self.arrays['ko'] * np.random.normal(-4, size=self.arrays['ko'].shape)
        diffs = []
        for i in range(self.args.iterations):
            last = self.arrays["gene"]
            self.arrays["gene"] = torch.toarray(self.model_pred(self.arrays['ko'], self.arrays["gene"]))
            diff = abs(self.arrays["gene"] - last).mean()
            diffs.append(diff)
            if diff < self.args.tolerance:
                io.log.info("Mean changes: " + str(np.asarray(diffs)))
                io.log.info("Reached convergence")
                return
            elif diff > 1e10:
                io.log.info("Mean changes: " + str(np.asarray(diffs)))
                io.log.warning("Diverges")
                # we don't keep gene matrix that has diverged
                del self.arrays["gene"]
                return
        io.log.info("Mean changes: " + str(np.asarray(diffs)))
        io.log.warning("Reached max iterations")

    
    def B2W_function(self):
        # manage args
        assert self.arrays['B'].shape == self.arrays['weight'].shape
        
        optimizer = torch.parse_optim(self.args.gradient_descent)([self.W])
        outputs = torch.train(self.model_B, [], optimizer, self.args.epochs, self.args.prints, config.epoch, self._abs_aps())
        self.arrays["weight"] = torch.toarray(self.W)
        
        # update current epoch for continued training
        config.epoch += self.args.epochs
        
        if "B2W" in self.arrays:
            self.arrays["B2W"] = pd.concat((self.arrays["B2W"], outputs))
        else:
            self.arrays["B2W"] = outputs


    def tfpk_function(self):
        try: self.parse_n_tf_pk()
        except argparse.ArgumentTypeError:
            if self.n_tf is None:
                raise argparse.ArgumentTypeError("n_tf could not be determined automatically")
        
        for name in self.args.name:
            self.arrays["tf_" + name] = panda.iloc(self.arrays[name])[:, :self.n_tf]
            self.arrays["pk_" + name] = panda.iloc(self.arrays[name])[:, self.n_tf:]
        

    # methods
    
    
    def _abs_aps(self):
        """
        If we know ground truth network, get a function to yield the average precision
        :return: None or function
        """
        if "edges" not in self.arrays: return None
        def aps():
            self.arrays["weight"] = torch.toarray(self.W)
            tf_ap, pk_ap = self.ap_abs()
            return {"TFAP": tf_ap, "PKAP": pk_ap}
        return aps
    
    
    def ap_abs(self):
        """
        Get average precision score for TF and PK separately.
        It is for abs values so it is performance of finding an edge regardless of edge sign.
        :return: float, float
        """
        edges = np.asarray(self.arrays['edges'])
        tf_edges = edges[:, :self.n_tf].ravel() != 0
        pk_edges = edges[:, self.n_tf:self.n_tf + self.n_pk].ravel() != 0
        tf_weight = abs(self.arrays['weight'][:, :self.n_tf].ravel())
        pk_weight = abs(self.arrays['weight'][:, self.n_tf:self.n_tf + self.n_pk].ravel())
        return average_precision_score(tf_edges, tf_weight), average_precision_score(pk_edges, pk_weight)
    
    
    def parse_n_tf_pk(self):
        self.set_n_tf(self.args.n_tf)
        self.set_n_pk(self.args.n_pk)
        # fall back on finding it from tf_ko and pk_ko files
        if not self.n_tf:
            try: self.n_tf = self.arrays["tf_ko"].shape[0]
            except KeyError:
                raise argparse.ArgumentTypeError("n_tf could not be determined")
        if not self.n_pk:
            try: self.n_pk = self.arrays["pk_ko"].shape[0]
            except KeyError:
                raise argparse.ArgumentTypeError("n_pk could not be determined")
    
    def B(self, W, I_TF, I_PK):
        """
        Get B from W
        :param W: shared variable 
        :param I_TF: 
        :param I_PK: 
        :return: W I_TF (2I - W I_PK)^-1
        :return: W I_TF (2I - 1/2 W I_PK W)^-1
        """
        n_gene = self.get_n_gene()
        
        if n_gene != self.n_tf + self.n_pk:
            pad = tc.zeros((n_gene, n_gene - self.n_tf - self.n_pk))
            W = tc.cat((W, pad), dim=1)
        
        I = tc.eye(n_gene)
        I_TF = torch.tensor(I_TF)
        I_PK = torch.tensor(I_PK)
        
        _B = W @ I_TF @ torch.inv(I - W @ I_PK)
        if self.Bdiag: _B = torch.fill_diagonal(_B, 0)
        return _B

    def UBX(self, X, W, I_TF, I_PK, ko):
        """
        Get U_k @ B @ x for each (U_k,x) 
        :param X: columns of observations aka matrix
        :param W: trainable weights
        :param I_TF: 
        :param I_PK: 
        :param ko: 
        :return: 
        """
        _BX = self.B(W, I_TF, I_PK) @ X
        return self.U(ko) * _BX

    def get_model_W(self, W, mask=None, sign_mask=None):
        # make sure diagonal is zero
        if self.Wdiag: _W = torch.fill_diagonal(W, 0)
        else: _W = W
        # remove edges not masked
        if mask is not None:
            _W = _W * torch.tensor(mask)
        # replace edges in sign_mask with versions forced to have a certain sign
        if sign_mask is not None:
            _W = _W * torch.tensor(sign_mask == 0) + abs(_W) * torch.tensor(sign_mask)
        return _W
        

    def get_model(self, W, reg_lambdas, mask, sign_mask, reg_mask, reg):

        def _model(ko, x):
            # x and ko are assumed to have 1 experiment in a column and 1 gene in a row
            ko, x = torch.tensor(ko), torch.tensor(x)
            _BX = self.UBX(x, self.get_model_W(W, mask, sign_mask), self.I_TF(), self.I_PK(), ko)
            X_pred = self.X(_BX, ko, x)

            return loss(self.UE(x, X_pred), self.n_tf, self.n_pk, W, self.I_TF(), self.I_PK(), reg_lambdas, reg_mask, reg)

        return _model

    def get_model_pred(self, W):

        def _model(ko, x):
            ko, x = torch.tensor(ko), torch.tensor(x)
            _BX = self.UBX(x, W, self.I_TF(), self.I_PK(), ko)
            return self.X(_BX, ko, x)

        return _model

    def get_model_B(self, true_B, W, reg_lambdas, mask, sign_mask, reg_mask, reg):
        """
        B -> W
        Given B we can get a model that describes the error to minimize in order to get W.
        :param true_B: a B matrix 
        :param W: torch trainable tensor
        :param reg: 
        :return: 
        """
        true_B = torch.tensor(true_B)

        def _model():
            _B = self.B(self.get_model_W(W, mask, sign_mask), self.I_TF(), self.I_PK())
            return loss(_B - true_B, self.n_tf, self.n_pk, W, self.I_TF(), self.I_PK(), reg_lambdas, reg_mask, reg)

        return _model

    # setters
    
    def set_n_pk(self, val):
        """
        Set n_pk and if possible n_tf using n_pk
        """
        if val is None: return
        self.n_pk = val
        if self.n_tf is None:
            self.set_n_tf(self.get_n_gene() - val)
    
    
    def set_n_tf(self, val):
        """
        Set n_pk and if possible n_tf using n_pk
        """
        if val is None: return
        self.n_tf = val
        if self.n_pk is None:
            self.set_n_pk(self.get_n_gene() - val)
    
    
    def set_W(self):
        """
        Prepare the tensor W for defining the network.
        :return: 
        """
        self.W = torch.tensor(self.arrays["weight"], requires_grad=True)
    
    
    # getters
    
    def get_mask(self):
        return self.mask(np.ones(self.arrays['weight'].shape))
    
    
    def get_n_gene(self):
        """
        Get the number of genes that are measured
        :return: int 
        """
        try: return self.arrays['gene'].shape[0]
        except KeyError: pass
        try: return self.arrays['ko'].shape[0]
        except KeyError: pass
        try: return self.arrays['B'].shape[0]
        except KeyError: pass
        try: return self.arrays['edges'].shape[0]
        except KeyError: return None
    
    
    def get_n_knockouts(self):
        """
        Get the number of datapoints (knockouts)
        :return: int 
        """
        try: return self.arrays['ko'].shape[1]
        except KeyError: return self.arrays['gene'].shape[1]
    
    
    def get_n_parameters(self):
        """
        Count number of trainable parameters by summing over all nonzero values in all masks.
        :return: int
        """
        return panda.sum(self.get_mask())
    
    def I_TF(self):
        """
        Assuming nodes are sorted to have TF and then PK we get I_TF which is the mask for TF rows.
        :return: array with ones in diagonal for each TF and zeros for each PK
        """
        return np.diag([1] * self.n_tf + [0] * (self.get_n_gene() - self.n_tf))
        
    def I_PK(self):
        """
        Assuming nodes are sorted to have TF and then PK we get I_PK which is the mask for PK rows.
        :return: array with ones in diagonal for each PK and zeros for each TF
        """
        return np.diag([0] * self.n_tf + [1] * self.n_pk + [0] * (self.get_n_gene() - self.n_tf - self.n_pk))
    
    @staticmethod
    def mask_TF(weight, mask):
        """
        Disallow connections in weight matrix based on TF mask.
        It is assumed that values in weight matrix is ordered with TF first and PK thereafter (along axis 1).
        :param weight: matrix where some values will be set to zero
        :param mask: Mask that is used to set values to zero.
        :return: weight matrix with some masked values set to zero.
        """
        assert mask.shape[0] == weight.shape[0], "the TF mask is not as tall as the weight is going to mask"
        _weight = weight[:, :mask.shape[1]]
        _weight[~mask] = 0
        weight[:, :mask.shape[1]] = _weight
        return weight
    
    @staticmethod
    def mask_PK(weight, mask, n_tf=None):
        """
        Disallow connections in weight matrix based on PK mask.
        It is assumed that values in weight matrix is ordered with TF first and PK thereafter (along axis 1).
        It is not assumed that there is no weights after TF, PK.
        :param weight: matrix where some values will be set to zero
        :param mask: Mask that is used to set values to zero.
        :param n_tf: Number of TFs. Used to find the starting column of the index that is masked.
        If not given it is assumed that the masking happens at the rightmost part of the array. 
        :return: weight matrix with some masked values set to zero.
        """
        assert mask.shape[0] == weight.shape[0], "the PK mask is not as tall as the weight is going to mask"
        if n_tf is None: n_tf = weight.shape[1] - mask.shape[1]
        _weight = weight[:, n_tf:n_tf+mask.shape[1]]
        _weight[~mask] = 0
        weight[:, n_tf:n_tf+mask.shape[1]] = _weight
        return weight
    
    @staticmethod
    def U(ko):
        """
        :param ko: 
        :return: matrix U where each column is the U_k, so no longer the diag matrix as in lit. 
        """
        return 1 - ko
    
    @staticmethod
    def UE(X, X_pred):
        """
        :param X: x_k column vectors aka X matrix
        :param X_pred: x_k column vectors aka X matrix
        :return: e_k error vectors aka E matrix
        """
        # isolating E in the expression makes this
        return X - X_pred
    
    @staticmethod
    def X(BX, ko, X):
        """
        Get a prediction/next time step of gene expression x using gene expression x.
        :param BX: 
        :param ko: 
        :return: 
        """
        # X is gene measurements, ko masks for the knockouts
        C = X * ko
        return BX + C


def mask(array, mask=None):
    """
    Remove diagonal in an array or use a masking array.
    :param array: array to edit.
    :param mask: optional mask to use, default is removing diagonal.
    :return: the given array now with zeros inserted according to masking.
    """
    if mask is None: np.fill_diagonal(array, 0)
    else: array[mask == 0] = 0
    return array


def loss(diff_tensor, n_tf, n_pk, W, I_T, I_P, reg_lambdas, reg_mask=None, reg=ar.l1):
    """
    Get loss with optional l-norm 1 included
    :param diff_tensor: error column vectors aka matrix. prediction minus target.
    :param n_tf: 
    :param n_pk: 
    :param W: tensor describing weights which can be used for regularization
    :param reg_lambdas: coefficient (often called lambda) used on regularization
    :param reg_mask: arrays with float values for the amount of regularization applied to each edge.
    A value of 1 is default. 0 is no regularization so should be used if we expect there to be an edge. 
    The value can also be larger than 1 if we are convinced there should be no edge there.
    :param reg: regularization function, default is l1-norm
    :return: list of losses, either [l2-norm, MSE] or [l2-norm + regularization, MSE]
    """
    l2 = ar.l2(diff_tensor)
    # number of scalar trainable weights
    n_W = W.shape[0] * W.shape[1]
    if not reg_lambdas["TF"] and not reg_lambdas["PK"]: return [l2, l2 / n_W]

    tf_W, pk_W = W[:, :n_tf], W[:, n_tf:n_tf + n_pk]
    
    if reg_mask is None:
        regularization = reg_lambdas["TF"] * reg(tf_W) + reg_lambdas["PK"] * reg(pk_W)
        if reg_lambdas["cascade"]:
            I_T = torch.tensor(I_T)
            I_P = torch.tensor(I_P)
            regularization += reg_lambdas["cascade"] * cascade(W, I_T, I_P).sum()
    else:
        reg_mask = torch.tensor(reg_mask)
        tf_reg_mask, pk_reg_mask = reg_mask[:, :n_tf], reg_mask[:, n_tf:n_tf + n_pk]
        # the reg function: array -> scalar
        regularization = reg_lambdas["TF"] * reg(tf_W[tf_reg_mask == 1]) + reg_lambdas["PK"] * reg(pk_W[pk_reg_mask == 1])
        # reward using known edges. in order to reward in loss and not get negative values
        # we use 1 - MichaelisMenten since Michaelis-Menten approaches 1.
        # this is also a nice function because it cares less and less about the size of a weight the greater it gets 
        # and we don't know that these edges should be large, we just know they should be there
        regularization += reg_lambdas["known"] * ((1-reg_mask) * (1 - ar.michaelis_menten(W))).sum()
  
    return [l2 + regularization, l2 / n_W]


if __name__ == '__main__':
    # instantiate a new Ebernet 
    main(Ebertorch())

